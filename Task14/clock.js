'use strict';

var centrX = 250;
var centrY = 250;
var clockR = 250;   //радиус циферблата
var digR = 30;      //радиус цифры
var fontSize = 32;
let lineHour;
let lineMin;
let lineSec;

//перевод градуса в радианы-------------
var rad = function toRadians(angle) {
    return angle * (Math.PI / 180);
}
//--------------------------------------

//событие после загрузки страницы-----------------------------------------
document.body.onload = function () {
    var SVGElem = document.getElementById("mainSvg");

    var circle = document.createElementNS("http://www.w3.org/2000/svg", 'circle');
    circle.setAttribute("stroke", "green");
    circle.setAttribute("fill", "yellow");
    circle.setAttribute("r", clockR);
    circle.setAttribute("cx", centrX);
    circle.setAttribute("cy", centrY);
    SVGElem.appendChild(circle);

    //формируем цифры часов------------------------------------------
    for (var i = 0; i < 12; i++) {
        let x, y;  //координаты центра текущей цифры
        x = centrX + (clockR - 1.5 * digR) * Math.cos(rad(30) * i - rad(60));
        y = centrY + (clockR - 1.5 * digR) * Math.sin(rad(30) * i - rad(60));

        let dig = document.createElementNS("http://www.w3.org/2000/svg", 'circle');
        dig.setAttribute("stroke", "green");
        dig.setAttribute("fill", "#eefeff");
        dig.setAttribute("r", digR);
        dig.setAttribute("cx", x);
        dig.setAttribute("cy", y);
        SVGElem.appendChild(dig);

        //вставляем цифру---------------------------
        var txt = document.createElementNS("http://www.w3.org/2000/svg", 'text');
        txt.setAttribute("x", x);
        txt.setAttribute("y", y + (fontSize / 3));
        txt.setAttribute("text-anchor", "middle");
        txt.setAttribute("font-size", fontSize);
        txt.style.fill = "dodgerblue";
        txt.textContent = i + 1;
        SVGElem.appendChild(txt);
        //--------------------------------------
    }
    //---------------------------------------------------------------

    //создаем элементы стрелок часов---------------------------------
    lineHour = document.createElementNS("http://www.w3.org/2000/svg", 'line');
    lineHour.setAttribute("x1", centrX);
    lineHour.setAttribute("y1", centrY + 0.1 * clockR);
    lineHour.setAttribute("x2", centrX);
    lineHour.setAttribute("y2", centrY - 0.45 * clockR);
    lineHour.setAttribute("stroke", "green");
    lineHour.setAttribute("stroke-width", "6");
    SVGElem.appendChild(lineHour);

    lineMin = document.createElementNS("http://www.w3.org/2000/svg", 'line');
    lineMin.setAttribute("x1", centrX);
    lineMin.setAttribute("y1", centrY + 0.1 * clockR);
    lineMin.setAttribute("x2", centrX);
    lineMin.setAttribute("y2", centrY - 0.65 * clockR);
    lineMin.setAttribute("stroke", "green");
    lineMin.setAttribute("stroke-width", "4");
    SVGElem.appendChild(lineMin);

    lineSec = document.createElementNS("http://www.w3.org/2000/svg", 'line');
    lineSec.setAttribute("x1", centrX);
    lineSec.setAttribute("y1", centrY + 0.1 * clockR);
    lineSec.setAttribute("x2", centrX);
    lineSec.setAttribute("y2", centrY - 0.9 * clockR);
    lineSec.setAttribute("stroke", "green");
    lineSec.setAttribute("stroke-width", "1");
    SVGElem.appendChild(lineSec);
    //---------------------------------------------------------------

    //создаем цифровые часы------------------------------------------
    var digitalClock = document.createElementNS("http://www.w3.org/2000/svg", 'text');
    digitalClock.setAttribute("x", centrX);
    digitalClock.setAttribute("y", centrY - 0.4*centrY);
    digitalClock.setAttribute("text-anchor", "middle");
    digitalClock.setAttribute("font-size", fontSize);
    digitalClock.style.fill = "dodgerblue";
    SVGElem.appendChild(digitalClock);

    //запускаем таймер-----------------------------------------------
    setInterval(updateTime, 1000);

    function updateTime() {
        var currTime = new Date();
        var hours = currTime.getHours();
        var minutes = currTime.getMinutes();
        var seconds = currTime.getSeconds();

        digitalClock.textContent = str0l(hours, 2) + ':' + str0l(minutes, 2) + ':' + str0l(seconds, 2);

        lineSec.setAttribute("transform", `rotate(${6 * seconds} ${centrX} ${centrY})`);
        lineMin.setAttribute("transform", `rotate(${6 * minutes + Math.ceil(0.1 * seconds)} ${centrX} ${centrY})`);
        lineHour.setAttribute("transform", `rotate(${30 * hours + Math.ceil(0.5 * minutes)} ${centrX} ${centrY})`);
    }

    // дополняет строку val слева нулями до длины Len
    function str0l(val, len) {
        var strVal = val.toString();
        while (strVal.length < len)
            strVal = '0' + strVal;
        return strVal;
    }
    //---------------------------------------------------------------
};