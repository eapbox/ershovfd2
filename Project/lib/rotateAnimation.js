$.fn.animateRotate = function(angleB, angle, duration, easing, complete) {
    return this.each(function() {
        var $elem = $(this);

        $({deg: angleB}).animate({deg: angle}, {
            duration: duration,
            easing: easing,
            step: function(now) {
                $elem.css({
                    transform: 'rotate(' + now + 'deg)'
                });
            },
            complete: complete || $.noop
        });
    });
};