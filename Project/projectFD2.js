'use strict';

//2. Это первое с чего стартует наше приложение.------------------------------------------
class ApplicationController {
    constructor(applicationView) {
        this.applicationView = applicationView;
        this.applicationView.render();  //3 - отрисовываем игровые "площадки"

        //5. оформляем поле отображения задания-----------------------------------
        var tasks = new TaskFieldController(
            new TaskFieldView(this.applicationView.fldTask),
            new TaskFieldModel()
        );
        //------------------------------------------------------------------------

        //4.Формируем поле расположения базовых элементов-------------------------
        var baseElements = new ElementsFieldController(
            new ElementsFieldView(this.applicationView.fldElements),  //где отрисовываем
            new ElementsFieldModel(),
            tasks
        );
        //------------------------------------------------------------------------

        //3.Формируем игровое поле: блок вставки фигуры;----------------------------
        //    кнопки:очистить поле, убрать элемент, повернуть элемент
        new BuildFieldController(
            new BuildFieldView(this.applicationView.fldBuild),         //где отрисовываем
            new BuildFieldModel(tasks, baseElements)
        );
        //------------------------------------------------------------------------
    }
}

//2.1. передаем элемент, где будем отрисовывать игровые площадки-------
//     поле сбора, поле задания, поле выбора элементов
class ApplicationView {
    constructor(element) {
        this.element = element;
    }

    //готовим элементы/площадки под размещение: -----------------------------------
    //поля сбора, поля задания, поля базовых элементов
    render() {
        var fieldBuildStyle = `position: fixed; top: 80px; height: 340px; width: 200px; background-color: dodgerblue; 
                               border-radius: 14px; border: solid 2px dodgerblue;`;

        var fieldTaskStyle = `position: fixed; top: 440px; height: 300px; width: 200px; background-color: #CAF2FF;
                              border-radius: 14px; border: solid 2px #CAF2FF;text-align:center;`;

        var fieldElemStyle = `position: absolute; top: 80px; left: 240px; height: auto; width: 520px; 
                              background-color: #CAF2FF;`;

        var bodyStyle = `margin: 0;padding: 0;font-family: 'Roboto', sans-serif;background-color: #eefeff;`;
        var headerStyle = `position: fixed; width: 100%;height: 60px;background-color: #0080FF;
                           box-shadow: 0px 5px 5px gray;margin: 0 0 5px 0;z-index: 2;opacity: 0.95;`;
        var headerMenuStyle = `position: relative;max-width: 760px;margin: 0 auto;
                               padding: 0;height: 100%;text-align: center;`;
        var headerMenuSpanStyle = `height: 60px;line-height: 60px;color: white;font-size: 28px;`;
        var contentStyle = `max-width: 760px;height: auto;position: relative;padding: 20px;
                            margin: 0 auto;background-color: white;`;


        //оформление страницы-------------------------------------------------------
        this.element.attr('style', bodyStyle);

        //заглавная полоса--------------------------
        var divH = $('<div class="header">');
        divH.attr('style', headerStyle);
        this.element.append(divH);

        var divHM = $('<div class="header-menu">');
        divHM.attr('style', headerMenuStyle);
        divH.append(divHM);

        var span = $('<span>SmartGame</span>');
        span.attr('style', headerMenuSpanStyle);
        divHM.append(span);
        //-----------------------------------------

        //контент----------------------------------
        var Content = $('<div class="content">');
        Content.attr('style', contentStyle);
        this.element.append(Content);
        //-----------------------------------------
        //--------------------------------------------------------------------------

        //Поле сбора задания---------------------------
        var fld = this.fldBuild = $('<div id="fldBuild">');
        fld.attr('style', fieldBuildStyle);
        Content.append(fld);
        //---------------------------------------------

        //Поле задания---------------------------------
        fld = this.fldTask = $('<div id="fldTask">');
        fld.attr('style', fieldTaskStyle);
        Content.append(fld);
        //---------------------------------------------

        //Поле базовых элементов-----------------------
        fld = this.fldElements = $('<div id="fldElements">');
        fld.attr('style', fieldElemStyle);
        Content.append(fld);
        //---------------------------------------------
    }
} //отрисовали площадки
//------------------------------------------------------------------------------------------

//3. создаем и оформляем поле выполнения задания---------------------------------------------
class BuildFieldController {
    constructor(view, model) {
        this.view = view;
        this.view.render();   //оформляем поле решения задачи
        this.model = model;
        this.model.config(this.view.receivField);  //настраиваем принимающий фигуры блок

        const rotateElem = (el) => this.model.rotateElem(el);
        const removeElem = (el) => this.model.removeElem(el);

        //кнопки-----------------------------------------------------------------------------------
        console.log(`назначаем события кнопкам`);
        $('#btnClearAll').click(function () {
            removeElem($('#receivField div'));
        });

        $('#btnRemove').click(function () {
            removeElem($('#receivField div:last-child'));
        });

        $('#btnRotate').click(function () {
            rotateElem($('#receivField div:last-child img'));
        });
        //----------------------------------------------------------------------------------------
    }
}

//3.1.Визуальное представление игрового поля--------------------------
class BuildFieldView {
    constructor(view) {
        this.view = view;
    }

    //оформляем область решения задачи-------
    render() {
        console.log(`оформляем область приема базовых элементов`);
        var receivField = $('<div id="receivField"></div>');
        receivField.css('position', 'absolute');
        receivField.css('width', '160px');
        receivField.css('height', '160px');
        receivField.css('top', '20px');
        receivField.css('left', '20px');
        receivField.css('background-color', 'white');
        receivField.css('box-shadow', 'inset 0px 0px 20px dodgerblue');
        receivField.css('border-radius', '10px');
        this.view.append(receivField);
        this.receivField = receivField; //назначаем принимающий блок

        console.log(`отрисовываем кнопки`);
        var btn = $('<button type="button" id="btnClearAll">Очистить</button>').button();
        btn.css('top', '190px');
        btn.css('left', '40px');
        btn.button('disable');
        this.view.append(btn);

        btn = $('<button type="button" id="btnRemove">Убрать</button>').button();
        btn.css('top', '200px');
        btn.css('left', '50px');
        btn.button('disable');
        this.view.append(btn);

        btn = $('<button type="button" id="btnRotate">Повернуть</button>').button();
        btn.css('top', '210px');
        btn.css('left', '35px');
        btn.button('disable');
        this.view.append(btn);
    }
}

//3.2.Модель игрового поля----------------------------------------------------
class BuildFieldModel {
    constructor(tasks, baseElements) {
        this.tasks = tasks;
        this.baseElements = baseElements;
    }

    //настраиваем принимающее поле----------------------------
    config(receivField) {
        const checkGame = () => this.tasks.model.checkGame();
        const baseElToBuild = () => this.baseElements.model.baseElToBuild();

        receivField.droppable({
            scope: 'BaseElement', //какие элементы ожидаем (общий scope)

            activate: function (Ev, UI) {
                console.log(`Началось перемещение`);
                receivField.css('background-color', '#c6ffe1');

            },
            deactivate: function (Ev, UI) {
                console.log(`не Помещен. Возвращаем назад`);
                UI.draggable.css('top', '0px');
                UI.draggable.css('left', '0px');
                receivField.css('background-color', 'white');
            },
            drop: function (Ev, UI) {
                console.log(`Помещен`);
                receivField.append(UI.draggable); //перемещаем
                UI.draggable.css('position', 'absolute');
                UI.draggable.css('top', '0px');
                UI.draggable.css('left', '0px');
                UI.draggable.css('width', '150px');
                UI.draggable.css('height', '150px');

                $('#btnRotate').button('enable');
                $('#btnRemove').button('enable');
                $('#btnClearAll').button('enable');
                //проверяем текущее расположение на решенность
                checkGame();
            }
        });
    }
    //------------------------------------------------------

    removeElem(curBaseElem) {
        curBaseElem.css('position', 'relative');
        curBaseElem.css('width', '120px');
        curBaseElem.css('height', '120px');
        $('#fldElements').append(curBaseElem); //убираем с поля сборки - назад на поле расположения

        if (!$("#receivField div")[0]) {
            $('#btnRotate').button('disable');   //скрываем кнопки
            $('#btnRemove').button('disable');
            $('#btnClearAll').button('disable');
        }
    }

    rotateElem(curBaseElem) {
        const checkGame = () => this.tasks.model.checkGame();

        let curRotate = curBaseElem.attr('rotate');
        //curBaseElem.rotate(parseInt(curRotate) + 90);  //без эффекта
        curBaseElem.parent().animateRotate(parseInt(curRotate),parseInt(curRotate) + 90);  //с эффектом

        if (curRotate == 270) {
            curRotate = 0;
            curBaseElem.attr('rotate', '0');
        } else
            curBaseElem.attr('rotate', `${parseInt(curRotate) + 90}`);

        checkGame(); //проверяем текущее расположение на решенность
    }
}
//--------------------------------------------------------------------------------------------

//4. создаем и оформляем поле расположения базовых элементов----------------------------------
class ElementsFieldController {
    constructor(view, model,tasks) {
        this.view = view;
        this.view.render();
        this.model = model;
        this.tasks = tasks;

        //6.Создаем базовые игровые элементы----------------------
        var countElem = 18;  //можно получать из базы
        for (var i = 0; i < countElem; i++) {
            console.log(`создаем элемент№: ${i + 1}`);
            new BaseElemController(
                new BaseElemModel((i+1), this.tasks),  //номер текущего базового элемента
                new BaseElemView(this.view.baseLocation)  //где отрисовываем
            );
        }
        //--------------------------------------------------------
    }
}

//4.1.Визуальное представление поля базовых элементов---------------
class ElementsFieldView {
    constructor(view) {
        this.view = view;

        this.baseLocation = this.view;
    }

    //оформляем область решения задачи-------
    render() {
        console.log(`оформляем область расположения базовых элементов`);

    }
}

//4.2.Модель игрового поля----------------------------------------------------
class ElementsFieldModel {
    constructor() {
        console.log(`модель базовых элементов`);
    }
}

//--------------------------------------------------------------------------------------------

//5. Создаем и оформляем поле расположения текущего задания-----------------------------------
class TaskFieldController {
    constructor(view, model) {
        this.view = view;
        this.model = model;
        this.view.render();
        this.view.renderTask(this.view.taskLocation, this.model.curTask);  //отрисовываем указанную задачу

        console.log(`назначаем событие кнопке-btnNextTask`);
        const setNextTask = () => this.setNextTask();
        const setNumTask = (numTask) => this.model.setNumTask(numTask);
        $('#btnNextTask').click(function () {
            setNextTask();
        });

        //событие изменения хэша в адресной строке---------------------
        window.onhashchange = function () {
            let hashNumTask = window.location.hash;
            let regHashTask = new RegExp(`#task`, 'g');
            if (hashNumTask.search(regHashTask) >= 0) {
                let curNumTask = hashNumTask.replace(regHashTask, '');
                setNumTask(curNumTask);
                setNextTask();
            }
        }
        //--------------------------------------------------------------
    }

    //устанавливаем и отрисовываем следующую задачу
    setNextTask() {
        this.view.renderTask(this.view.taskLocation, this.model.curTask);
    }
}

//5.1.Визуальное представление поля текущей задачи-----------
class TaskFieldView {
    constructor(view) {
        this.view = view;
    }

    //оформляем область расположения текущей задачи
    render() {
        console.log(`оформляем область расположения текущей задачи`);
        var textStyle = `height: 60px;line-height: 60px; color: #0080FF; font-size: 28px;`;

        var numTaskLocation = $('<span p id="numTaskLocation"></spanp>');
        numTaskLocation.attr('style', textStyle);
        this.view.append(numTaskLocation);
        this.numTaskLocation = numTaskLocation;

        var taskLocation = $('<div id="taskLocation"></div>');
        taskLocation.css('position', 'absolute');
        taskLocation.css('width', '160px');
        taskLocation.css('height', '160px');
        taskLocation.css('left', '20px');
        taskLocation.css('background-color', 'white');
        taskLocation.css('border-radius', '10px');
        this.view.append(taskLocation);
        this.taskLocation = taskLocation;  //назначаем блок отрисовки задачи

        console.log(`отрисовываем кнопку следующей задачи`);
        var btn = $('<button type="button" id="btnNextTask">Следующая</button>').button();
        btn.css('top', '190px');
        btn.button('disable');
        this.view.append(btn);
    }

    //отрисовываем текущую задачу---------------
    renderTask(taskLocation, taskNum) {
        console.log(`Отрисовываем задачу № ${taskNum}`);
        this.numTaskLocation.text(`Задача № ${taskNum}`);

        $("#taskLocation").empty();  //очищаем поле задания
        $('#btnClearAll').button('enable');
        $('#btnClearAll').click();   //очищаем поле сборки задачи
        $('#btnClearAll').button('disable');
        $('#btnNextTask').button('disable');  //скрываем копку-переход к следующему заданию


        var curTaskStyle = `height: 150px; width: 150px; 
                             border: solid 2px mediumblue; 
                             line-height: 150px;
                             padding: 2.5%;
                             margin: 4px; display: inline-block;
                             box-sizing: border-box;cursor: pointer;
                             border-radius: 10px;`;

        var curTaskEl = $(`<div id="curTaskEl">`);  //блок расположения текущей задачи
        curTaskEl.attr('style', curTaskStyle);
        curTaskEl.html(`<img src="taskSVG/task${taskNum}.svg" width="100%" height="100%" alt="task${taskNum}">`);
        taskLocation.append(curTaskEl);
        curTaskEl.fadeTo(0, 0);
        curTaskEl.fadeTo(1000, 1);
    }
}

//5.2.Модель поля задания-------------------------------------
class TaskFieldModel {
    constructor() {
        console.log(`модель поля задания`);

        if ('fd2TaskNum' in localStorage) {
            this.curTask = parseInt(localStorage['fd2TaskNum']);
        } else {
            this.curTask = 1;   //первая задача
        }
        //this.curTask = 9;   //для сброса стартового задания
        localStorage.setItem('fd2TaskNum',parseInt(this.curTask));
    }

    setNumTask (numTask) {
        this.curTask = numTask;
        localStorage.setItem('fd2TaskNum',parseInt(this.curTask));
    }

    //получаем решение указанной задачи---------------------
    getTaskSolve(numTask) {
        let dataRes;

        $.ajax("Solves.json", {
            async: false,
            "type": "Get",
            "dataType": "JSON",  //ожидаемый тип данных
            "success": function (data) {
                dataRes = data[`task${numTask}`];
            },
            "error": function (jqXHR, statusStr, errorStr) {
                alert(statusStr + ' ' + errorStr);
            }
        });

        return dataRes; //решение - массив с элементами и поворотом
    }
    //----------------------------------------------------

    //Проверяем на решенность задачи----------------------
    checkGame() {
        let curSolve = $("#receivField div img");
        let isSolved = false;
        const getTaskSolve = (numTask) => this.getTaskSolve(numTask);
        let curTaskSolve = getTaskSolve(this.curTask);       //получаем решение текущей задачи
        if (curTaskSolve.length == curSolve.length) {
            //перебираем выбранные базовые элементы-
            for (var i = 0; i < curSolve.length; i++) {
                let curSolveBaseEl = curSolve[i].getAttribute('numBaseEl');
                let curSolveRotate = curSolve[i].getAttribute('rotate');

                let curTaskSolveBaseEl = curTaskSolve[i].baseEl;
                let curTaskSolveRotate = curTaskSolve[i].rotate;

                console.log(`curSolveBaseEl:     ${curSolveBaseEl};     curSolveRotate:     ${curSolveRotate}`);
                console.log(`curTaskSolveBaseEl: ${curTaskSolveBaseEl}; curTaskSolveRotate: ${curTaskSolveRotate}`);

                if (curSolveBaseEl != curTaskSolveBaseEl || curSolveRotate != curTaskSolveRotate) {
                    isSolved = false;
                    break;
                }
                isSolved = true;
            }
            //--------------------------------------

            //Успешное решение текущей задачи---------
            if (isSolved) {
                this.curTask = parseInt(parseInt(this.curTask) + 1);   //устанавливаем номер след. задания
                localStorage.setItem('fd2TaskNum',parseInt(this.curTask));

                $('#btnNextTask').button('enable');   //разрешаем кнопку-переход к следующей задаче
                $('#btnRotate').button('disable');    //скрываем кнопки управления элементами
                $('#btnRemove').button('disable');
                $('#btnClearAll').button('disable');
                $("#receivField").animateRotate(0,360,1000); //вращаем собранный блок с эффектом
            }
            //----------------------------------------
        }
        return isSolved;
    }
    //--------------------------------------------------
}

//--------------------------------------------------------------------------------------------

//6. Создаем базовые игровые элементы---------------------------------------------------------
class BaseElemController {
    constructor(model, view) {
        this.model = model;
        this.view = view;
        this.view.render(this.model.modelNum);  //отрисовываем нужный базовый елемент

        //настраиваем поведение базовой модели при перемещении/двойном клике--
        this.view.baseEl.draggable({
            scope: 'BaseElement',
            containment: 'false'
        });

        this.view.baseEl.dblclick(() => {
            this.model.baseElToBuild(this.view.baseEl);  //перемещаем элемент на поле сборки
        });

        this.view.baseEl.click(() => { //для мобильных версий
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                this.model.baseElToBuild(this.view.baseEl); //перемещаем элемент на поле сборки
            }
        });
        //--------------------------------------------
    }
}
//----------------------------------------------------------------

//6.1.Отрисовка нужного базового элемента-------------------------
class BaseElemView {
    constructor(element) {
        this.element = element;
    }

    render(modelNum) {
        console.log(`  - отрисовываем элемент№: ${modelNum}`);
        var baseElemStyle = `height: 120px; width: 120px; 
                             border: solid 4px rgba(0,0,0,0.3); 
                             margin: 4px; display: inline-block;
                             box-sizing: border-box;cursor: pointer;
                             border-radius: 10px;
                             box-shadow:  0px 0px 15px rgba(0,0,0,0.4);`;


        this.baseEl = $(`<div id="baseElem${modelNum}">`);
        this.baseEl.attr('style', baseElemStyle);
        this.baseEl.html(`<img src="baseElemSVG/baseElem${modelNum}.svg" width="95%" style="position: absolute; top: 2.5%; left: 2.5%" height="95%" alt="baseElem${modelNum}" rotate="0" numBaseEl="${modelNum}">`);
        //this.baseEl.html(`<img src="baseElemSVG/baseElem${modelNum}.svg" width="95%" style="position: absolute; top: 2.5%; left: 2.5%" height="95%" alt="baseElem${modelNum}" rotate="0" numBaseEl="${modelNum}"><span>${modelNum}</span>`);

        $(this.element).append(this.baseEl);
    }
}
//--------------------------------------------------------------

//6.2. - модель базовых элементов - перемещение-----------------
class BaseElemModel {
    constructor(modelNum,tasks) {
        this.modelNum = modelNum;
        this.tasks = tasks;
    }

    //перемещаем базовый элемент на поле сборки----------------
    baseElToBuild(baseEl) {
        var receivField = $("#receivField");
        receivField.append(baseEl); //перемещаем
        baseEl.css('position', 'absolute');
        baseEl.css('top', '0px');
        baseEl.css('left', '0px');
        baseEl.css('width', '150px');
        baseEl.css('height', '150px');

        $('#btnRotate').button('enable');
        $('#btnRemove').button('enable');
        $('#btnClearAll').button('enable');
        //проверяем текущее расположение на решенность
        this.tasks.model.checkGame();
    }
    //---------------------------------------------------------
}
//-----------------------------------------------------------------------------------------------------

//1. start SPA-----------------------------------------------------------------------------------------
var smartGame = new ApplicationController(new ApplicationView($("body")));
//var smartGame = new ApplicationController(new ApplicationView($(".content")));
//-----------------------------------------------------------------------------------------------------
