//Task18 Clock canvas Ershov
'use strict';
var centrX = 250;
var centrY = 250;
var clockR = 250;   //радиус циферблата
var digR = 30;      //радиус цифры
var fontSize = 32;

var clock = document.getElementById('clock');
var context = clock.getContext('2d');

//событие после загрузки страницы-----------------------------------------
document.body.onload = function () {
    context.textAlign = 'center';
    context.font = '27px Arial';

    setTimeout(updateTime, 1000);  //запускаем таймер
}
//------------------------------------------------------------------------

function updateTime() {
    clearCanvas(clock);    //очищаем сцену

    var currTime = new Date();     //получаем текущее время
    drowClock(currTime);           //отрисовываем часы под указанное время
    setTimeout(updateTime, 1000);
}

function drowClock(currTime) {
    //рисуем циферблат------------------
    context.fillStyle = '#7cb5dc';
    context.arc(centrX, centrY, clockR, 0, Math.PI * 2, false);
    context.fill();
    //----------------------------------

    //формируем цифры часов------------------------------------------
    for (var i = 0; i < 12; i++) {
        let crd = coord(deg(30) * i - deg(60),0.8*clockR);
        let x = centrX + crd.x;
        let y = centrY + crd.y;

        context.fillStyle = '#eefeff'; //отрисовываем кружки часов
        context.beginPath();
        context.arc(x, y, digR, 0, Math.PI * 2, false);
        context.fill();

        context.fillStyle = '#1873cd';      //отрисовываем цифры часов
        context.fillText((i + 1), x, y + digR / 3);
    }
    //---------------------------------------------------------------

    var hours = currTime.getHours();
    var minutes = currTime.getMinutes();
    var seconds = currTime.getSeconds();

    //отрисовываем цифровые часы------
    var digTimeStr = str0l(hours, 2) + ':' + str0l(minutes, 2) + ':' + str0l(seconds, 2);
    context.fillStyle = '#1873cd';
    context.fillText(digTimeStr, centrX, centrY - clockR / 2);

    //отрисовываем секундную стрелку---------------
    context.strokeStyle = '#507591';
    context.lineCap='round';
    context.lineWidth = 1;
    context.beginPath();
    context.moveTo(centrX,centrY);
    var crd = coord(deg(6*seconds - 90),0.95*clockR);
    context.lineTo(centrX+crd.x,centrY+crd.y);
    context.stroke();
    context.closePath();
    //---------------------------------------------

    //отрисовываем минутную стрелку----------------
    context.lineWidth = 4;
    context.beginPath();
    context.moveTo(centrX,centrY);
    crd = coord(deg(6*minutes + Math.ceil(0.1 * seconds)-90),0.7*clockR);
    context.lineTo(centrX+crd.x,centrY+crd.y);
    context.stroke();
    context.closePath();
    //---------------------------------------------

    //отрисовываем часовую стрелку-----------------
    context.lineWidth = 6;
    context.beginPath();
    context.moveTo(centrX,centrY);
    crd = coord(deg(30 * hours + Math.ceil(0.5 * minutes)-90),0.45*clockR);
    context.lineTo(centrX+crd.x,centrY+crd.y);
    context.stroke();
    context.closePath();
    //---------------------------------------------
}

//----------------------------------------------------------------------------

//определяем координаты на окружности радиуса R при угле radian----------
function coord(rdn,R) {
    return {x:R * Math.cos(rdn),y:R * Math.sin(rdn)};
}
//-------------------------------------------------------------

function clearCanvas(scene) {
    var context = scene.getContext('2d');
    context.fillStyle = '#FFFFFF';
    context.fillRect(0, 0, scene.width, scene.height);
}

// дополняет строку val слева нулями до длины Len
function str0l(val, len) {
    var strVal = val.toString();
    while (strVal.length < len)
        strVal = '0' + strVal;
    return strVal;
}
//-----------------------------------------------

//перевод градуса в радианы-------------
var deg = function toRadians(angle) {
    return angle * (Math.PI / 180);
}
//--------------------------------------
