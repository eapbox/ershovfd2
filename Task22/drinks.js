//В проекте DRINKS разработать класс TAJAXStorage
//Ershov
'use strict';

//**********************************************************
function TAJAXStorage(name) {
    var self = this;
    let prefix = 'ErshovTask22';
    let ErshovAllKeys = 'ALL_KEYS';
    let ajaxHandlerScript = "http://fe.it-academy.by/AjaxStringStorage2.php";
    let updatePassword = 'ErshovTask22_updatePassword';
    self.name = name;
    self.reg = new RegExp(`${self.name}_`, 'g');


    //Сохранить под ключем новое значение-----------------------
    self.addValue = function (key, value) {
        if (saveKey(key, value)) {
            //новый ключ добавляем в список ключей
            let allKeys = self.getValue(ErshovAllKeys);
            let newAllKeys = { };
            if (allKeys != undefined) {
                console.log(`списка ключей нет`)
                newAllKeys = allKeys;
            }

            newAllKeys[`${key}`] = '1';
            console.log(`Сохраняем список ключей`);
            console.log(`SaveAllKeys: ${JSON.stringify(newAllKeys)};`);
            saveKey(ErshovAllKeys, newAllKeys);
        }
    }
    //-----------------------------------------------------------

    //получить значение по ключу---------------------------------
    self.getValue = function (key) {
        return readValue(key);
    }
    //-----------------------------------------------------------

    //Помечаем ключ как удаленный--------------------------------
    self.deleteValue = function (key) {
        let curKey = readValue(key);
        if (curKey != undefined) {
            saveKey(key,'');   //пересохраняем ключ с пустым значением

            //Удаляем из списка ключей------------------
            let allKeys = self.getValue(ErshovAllKeys);
            delete allKeys[`${key}`];
            saveKey(ErshovAllKeys, allKeys);
            //------------------------------------------
            return true;
        } else {
            return false;//такого ключа нет
        }
    }
    //-----------------------------------------------------------

    //получаем список ключей--------------------------------
    self.getKeys = function () {
        let allKeys = readValue(ErshovAllKeys);
        return allKeys;
    }
    //-------------------------------------------------------

    //получить значение по ключу-----------------------------
    function readValue(key) {
        let dataRes;
        $.ajax(ajaxHandlerScript, {
            async: false,
            type: 'POST', dataType: 'json',
            data: {
                f: 'READ',
                n: `${prefix}_${self.name}_${key}`
            },
            success: function (data) {
                if (data.result == "") {
                    //записи с таким ключем в базе нет
                    console.log(`ключ: ${prefix}_${self.name}_${key} - отсутствует в базе`);
                } else if (data.result != "") {
                    //успешное получение данных по ключу
                    console.log(`ключ: ${prefix}_${self.name}_${key} - есть в базе`);
                    console.log(`val = ${data.result}`);
                    dataRes = JSON.parse(data.result);
                }
            },
            error: errorHandler
        });

        console.log("getValue_end");
        return dataRes;
    }
    //-----------------------------------------------------------------------------------

    //Сохраняем требуемый ключ с нужным значением----------------------------------------
    function saveKey(key, value) {
        let isSuccessSave = false;
        $.ajax(ajaxHandlerScript, {
            async: false,
            type: 'POST', dataType: 'json',
            data: {
                f: 'INSERT',
                n: `${prefix}_${self.name}_${key}`,
                v: JSON.stringify(value)
            },
            success: function (data) {
                if (data.error != undefined) {
                    //запись с таким ключем в базе уже есть
                    console.log(`ключ: ${prefix}_${self.name}_${key} - уже есть в базе;`);
                    updateKey(key, value); //обновляем значение
                } else if (data.result != "") {
                    //новый ключ успешно добавлен в базу
                    console.log(`ключ: ${prefix}_${self.name}_${key} - успешно создан (${data.result})`);
                    isSuccessSave = true;
                }
            },
            error: errorHandler
        });
        return isSuccessSave;
    }

    //-------------------------------------------------------

    //обновляем значение существующего ключа-----------------
    function updateKey(key, value) {
        $.ajax(ajaxHandlerScript, {
            async: false,
            type: 'POST', dataType: 'json',
            data: {
                f: 'LOCKGET',
                n: `${prefix}_${self.name}_${key}`,
                p: updatePassword
            },
            success: function (data) {
                if (data.error != undefined) {
                    console.log(`ключ: ${prefix}_${self.name}_${key} - не доступен; ${data.error}`);
                } else {
                    //запись заблокирована - перезаписываем значение
                    console.log(`Обновляем ключ: ${prefix}_${self.name}_${key}`);
                    $.ajax(ajaxHandlerScript, {
                        async: false,
                        type: 'POST', dataType: 'json',
                        data: {
                            f: 'UPDATE',
                            n: `${prefix}_${self.name}_${key}`,
                            v: JSON.stringify(value),
                            p: updatePassword
                        },
                        success: function (data) {
                            if (data.error != undefined)
                                alert(`не удалось обновить данные; ${data.error}`);
                            console.log(`   -ключ: ${key} - обновлен`);
                        },
                        error: errorHandler
                    });
                    //----------------------------------------------
                }
            },
            error: errorHandler
        });
    }

    //--------------------------------------------------------

    function errorHandler(jqXHR, statusStr, errorStr) {
        alert(statusStr + ' ' + errorStr);
    }
}
//******************************************************************************************

var drinkStorage = new TAJAXStorage('drink');
var dishStorage = new TAJAXStorage('dish');
var messageDrink = document.getElementById('message1');
var messageDish = document.getElementById('message2');


//Drinks-----------------------------------------------------------------------------------
function createDrink() {
    let name = prompt('Введите название напитка: ');
    let isAlch = confirm('Алкогольный?');
    let recipe = prompt('Рецепт приготовления: ');

    drinkStorage.addValue(name, {isAlch: isAlch.toString(), recipe: recipe.toString()});
}

function getInfoDrink() {
    let name = prompt('Введите название напитка: ');
    let drink = drinkStorage.getValue(name);

    console.log(`получили данные`);
    if (drink) {
        messageDrink.innerHTML = `напиток: ${name}&#13;&#10;алкогольный: ${drink.isAlch}&#13;&#10;рецепт приготовления:&#13;&#10;${drink.recipe}`;
        return;
    }
    messageDrink.innerHTML = `напитка: ${name} - нет!`;
}

function deleteDrink() {
    let name = prompt('Введите название напитка: ');
    if (drinkStorage.deleteValue(name)) {
        messageDrink.innerHTML = `напиток: ${name} - успешно удален!`;
        return;
    }
    messageDrink.innerHTML = `напитка: ${name} - нет!`;
}

function listDrinks() {
    let drinks = drinkStorage.getKeys();
    let list = '';
    for (var el in drinks) {
        list += el + '&#13;&#10;';
    }
    if (list == '') {
        messageDrink.innerHTML = `Нет напитков`;
        return;
    }
    messageDrink.innerHTML = list;
}
//-----------------------------------------------------------------------------------------

//Dishs------------------------------------------------------
function createDish() {
    let name = prompt('Введите название блюда: ');
    let isMeet = confirm('Мясное?');
    let recipe = prompt('Рецепт приготовления: ');

    dishStorage.addValue(name, {isMeet:isMeet.toString(), recipe: recipe.toString()});
}

function getInfoDish() {
    let name = prompt('Введите название блюда: ');
    let dish = dishStorage.getValue(name);

    if (dish) {
        messageDish.innerHTML = `блюдо: ${name}&#13;&#10;мясное: ${dish.isMeet}&#13;&#10;рецепт приготовления:&#13;&#10;${dish.recipe}`;
        return;
    }
    messageDish.innerHTML = `блюда: ${name} - нет!`;
}

function deleteDish() {
    let name = prompt('Введите название блюда: ');
    if (dishStorage.deleteValue(name)) {
        messageDish.innerHTML = `блюдо: ${name} - успешно удалено!`;
        return;
    }
    messageDish.innerHTML = `блюда: ${name} - нет!`;
}

function listDishs() {
    let dishs = dishStorage.getKeys();
    let list = '';
    for (var el in dishs) {
        list +=el + '&#13;&#10;';
    }
    if (list == '') {
        messageDish.innerHTML = `Нет блюд`;
        return;
    }
    messageDish.innerHTML = list;
}
//-------------------------------------------------------------------------



