//Task_23. Создать проект ENCYCLO (Энциклопедия).
//Ershov
'use strict';
var body = document.body;
let hashArticle = 'article';   //базовое имя хэша статей
let regHashArticle = new RegExp(`#${hashArticle}_`, 'g');


//событие после загрузки страницы-----------------------------------------
document.body.onload = function () {
    let newHash = window.location.hash;
    showHashPage(newHash);  //формируем главную страницу
}
//------------------------------------------------------------------------

//событие изменения хэша в адресной строке--------------------------------
window.onhashchange = function () {
    let newHash = window.location.hash;
    showHashPage(newHash);
}

function showHashPage(newHash) {
    //если статья--------------
    if (newHash.search(regHashArticle) >= 0) {
        let numArticle = newHash.replace(regHashArticle, '')
        showArticle(body, numArticle);
    } else {
        switch (newHash) {
            case '#contents':
                showContents(body);
                break;
            case '':
                showMainPage(body);
                break;
        }
    }
}
//------------------------------------------------------------------------

//генерация страниц----------------------------------------------------------
function showMainPage(element) {
    //содержимое страницы можно было бы вытаскивать и из отдельного файла
    element.innerHTML = `
       <h2 style="text-align: center; color: green;">Энциклопедия</h2>
       <a href="#contents" style="display: block; margin: 0 auto; text-align: center;">Список статей</a>
    `;
}

function showContents(element) {
    //содержимое страницы можно было бы вытаскивать и из отдельного файла
    element.innerHTML = `
       <h2 style="text-align: center; color: dodgerblue;">Оглавление</h2>
       <ul>
        <li>
          <a href="#${hashArticle}_1">Статья 1</a>
        </li>
        <li>
          <a href="#${hashArticle}_2">Статья 2</a>
        </li>
        <li>
          <a href="#${hashArticle}_3">Статья 3</a>
        </li>
        <li>
          <a href="#${hashArticle}_4">Статья 4</a>
        </li>
        <li>
          <a href="#${hashArticle}_5">Статья 5</a>
        </li>
        <li>
          <a href="#${hashArticle}_6">Статья 6</a>
        </li>
       </ul>
    `;
}

function showArticle(element, num) {
    element.innerHTML = ``;  //очищаем

    let div1 = document.createElement('div');
    div1.style.width = '20%';
    div1.style.float = 'left';
    body.appendChild(div1);
    showContents(div1);

    let div2 = document.createElement('div');
    div2.style.backgroundColor = '#CAF2FF';
    div2.style.display = 'inline';
    body.appendChild(div2);

    div2.innerHTML = `
       <h2 style="text-align: center; color: green;">Статья: ${num}</h2>
       
       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus posuere gravida orci in faucibus. Aliquam sapien est, rhoncus eget velit eget, rutrum sagittis nulla. Mauris cursus velit enim, nec congue ante iaculis in. Nunc tincidunt pretium sem, nec commodo nunc consequat at. Aliquam ultrices lorem vitae odio laoreet, ut tempus est vehicula. Donec rhoncus, neque porttitor vestibulum convallis, quam orci pulvinar augue, eget pellentesque est leo vel augue. Vivamus ac urna turpis. Ut a nisl vel lacus accumsan elementum vel ac eros. Phasellus feugiat ultricies scelerisque. Morbi eleifend ac augue eu laoreet. Nullam posuere nunc id sem ultrices, a malesuada felis ultrices. Cras vitae nibh vel nunc fringilla aliquam a sit amet velit. Vivamus rutrum condimentum sodales. Fusce ornare urna nibh. Cras placerat purus in urna vulputate feugiat. Maecenas sed aliquet urna.</p>
    `;
}
//---------------------------------------------------------------------------


