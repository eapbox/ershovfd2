// Ершов Александр
// Task8_d1
/*
Переписать (подсчёт гласных букв в строке) без использования циклов, двумя способами:
с использованием метода массива forEach,
с использованием метода массива filter,
с использованием метода массива reduce.
*/
'use strict';

let str = prompt('Введите строку');
let vowels = {а:'а', о:'о', э:'э', и:'и', у:'у', ы:'ы', е:'е', ё:'ё', ю:'ю', я:'я'};

function getCountVowels(text) {
    var countVowels = 0;

    for (var symbol of text) {
        if (symbol.toLowerCase() in vowels)
            countVowels++;
    }
    return countVowels;
}

function showCountVowels1(text) {
    let countVowels = 0;

    text.split("").forEach(function(symbol,i,a){
        if (symbol.toLowerCase() in vowels)
            countVowels++;
    });

    alert(`гласных букв (forEach): ${countVowels}`);
}

function showCountVowels2(text) {
    let countVowels = (text.split("").filter(function(symbol,i,a){
        return (symbol.toLowerCase() in vowels)
    })).length;

    alert(`гласных букв (filter): ${countVowels}`);
}

function showCountVowels3(text) {
    let countVowels = text.split("").reduce(function(r,symbol,i,a){
        if (symbol.toLowerCase() in vowels)
            return r += 1;
        return r;
    },0);

    alert(`гласных букв (reduce): ${countVowels}`);
}

showCountVowels1(str);
showCountVowels2(str);
showCountVowels3(str);