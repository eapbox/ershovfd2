// Ершов Александр
// Task8_d3

'use strict';
var formDef1 =
    [
        {label: 'Название сайта:', kind: 'longtext', name: 'sitename'},
        {label: 'URL сайта:', kind: 'longtext', name: 'siteurl'},
        {label: 'Посетителей в сутки:', kind: 'number', name: 'visitors'},
        {label: 'E-mail для связи:', kind: 'shorttext', name: 'email'},
        {
            label: 'Рубрика каталога:', kind: 'combo', name: 'division',
            variants: [{text: 'здоровье', value: 1},
                       {text: 'домашний уют', value: 2},
                       {text: 'бытовая техника',value: 3
            }]
        },
        {
            label: 'Размещение:', kind: 'radio', name: 'payment',
            variants: [{text: 'бесплатное', value: 1}, {text: 'платное', value: 2}, {text: 'VIP', value: 3}]
        },
        {label: 'Разрешить отзывы:', kind: 'check', name: 'votes'},
        {label: 'Описание сайта:', kind: 'memo', name: 'description'},
        {label: 'Опубликовать:', kind: 'submit'},
    ];

var formDef2 =
    [
        {label: 'Фамилия:', kind: 'longtext', name: 'lastname'},
        {label: 'Имя:', kind: 'longtext', name: 'firstname'},
        {label: 'Отчество:', kind: 'longtext', name: 'secondname'},
        {label: 'Возраст:', kind: 'number', name: 'age'},
        {label: 'Зарегистрироваться:', kind: 'submit'},
    ];

var form1 = document.forms['Form1'];
var form2 = document.forms['Form2'];

//**************************************************************************
function createForm(frm, formDef) {
    formDef.forEach(function (v, i, a) {
        let text = document.createElement('span');

        switch (v.kind) {
            case 'longtext':
            case 'number':
            case 'shorttext':
            case 'check':
                let inp = document.createElement('input');

                text.innerHTML = `${v.label}  `;
                frm.appendChild(text);

                if (v.kind == 'longtext')
                    inp.setAttribute('type', 'text');
                if (v.kind == 'number')
                    inp.setAttribute('type', 'number');
                if (v.kind == 'shorttext')
                    inp.setAttribute('type', 'email');
                if (v.kind == 'check')
                    inp.setAttribute('type', 'checkbox');

                inp.setAttribute('name', v.name);
                frm.appendChild(inp);

                frm.appendChild(document.createElement('br'));
                break;
            case 'memo':
                let txtarea = document.createElement('textarea');

                text.innerHTML = `${v.label}  `;
                frm.appendChild(text);
                frm.appendChild(document.createElement('br'));

                txtarea.setAttribute('name', v.name);
                frm.appendChild(txtarea);

                frm.appendChild(document.createElement('br'));
                break;
            case 'combo':
                let select = document.createElement('select');
                let variants = v.variants;
                text.innerHTML = `${v.label}  `;
                frm.appendChild(text);

                select.setAttribute('name', v.name);
                select.setAttribute('style', 'width: 204px; margin-left: 2px');
                variants.forEach(function (v) {
                    select.innerHTML += `<option value="${v.value}">${v.text}</option>`;
                })
                frm.appendChild(select);

                frm.appendChild(document.createElement('br'));
                break;
            case 'radio':
                let variants_radio = v.variants;
                text.innerHTML = `${v.label}  `;
                frm.appendChild(text);

                variants_radio.forEach(function (v_r,i_r,a_r) {
                    let span = document.createElement('span');
                    let radio = document.createElement('input');

                    radio.setAttribute('type', 'radio');
                    radio.setAttribute('name', v.name);
                    radio.setAttribute('value', v_r.value);
                    frm.appendChild(radio);

                    span.innerHTML = v_r.text;
                    frm.appendChild(span);
                })
                frm.appendChild(document.createElement('br'));
                break;
            case 'submit':
                let sub = document.createElement('input');
                sub.setAttribute('type', 'submit');
                sub.setAttribute('value', v.label);

                frm.appendChild(sub);
                break;
        }
    });
}
//**************************************************************************
createForm(form1, formDef1);
createForm(form2, formDef2);
//**************************************************************************