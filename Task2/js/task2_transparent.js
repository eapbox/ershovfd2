'use strict';   //строгий режим js
function randomDiap(N,M) {
    return Math.floor(
        Math.random()*(M-N+1))+N;
}

var n = randomDiap(3,9);
var m = randomDiap(3,9);

var array = new Array();
for (var i=0; i < n; i++) {
    array[i] = new Array();
    for (var j=0; j < m; j++) {
        array[i][j] = randomDiap(1,9);
    }
}

var arrayTransp = new Array();
for (var i=0; i < m; i++) {
    arrayTransp[i] = new Array();
    for (var j=0; j < n; j++) {
        arrayTransp[i][j] = array[j][i];
    }
}

function createTable(a) {
    let n = a.length;
    let m = a[0].length;
    let row;
    let cell;

    let parentElem = document.body;

    let newTable = document.createElement('table');
    newTable.style.border = 'solid 1px black';
    newTable.style.borderCollapse = 'collapse';
    newTable.style.margin = '0 auto';
    newTable.style.marginTop = '20px';
    for (var i=0; i<n; i++) {
        row =newTable.insertRow(i);
        for (var j=0; j<m; j++) {
            cell = row.insertCell(j);
            cell.innerHTML = `${a[i][j]}`;
            if (j == i) {
                cell.style.backgroundColor = 'red';
                cell.style.color = 'white';
            }
            cell.style.border ='solid 1px black';
            cell.style.width = '40px';
            cell.style.height = '40px';
            cell.style.textAlign = 'center';
        }
    }
    // parentElem.innerHTML = `<p>Транспанированная матрица</p>`;
    parentElem.appendChild(newTable);
}

createTable(array);
createTable(arrayTransp);