// Ершов Александр
// Занятие №2 , задание5
/*5. Сгенерировать массив из N случайных целых чисел.
    Поменять местами все элементы относительно середины массива по следующей схеме:
    Было [1,2,3,4,5,6,7], стало [5,6,7,4,1,2,3]. Создавать новые массивы нельзя.*/
'use strict';   //строгий режим js
var n = 7;
var array = [];

function randomDiap(N,M) {
    return Math.floor(
        Math.random()*(M-N+1))+N;
}

for (var i = 0; i < n; i++) {
    array[i] = randomDiap(1, 10);
}
console.log(`original array: [${array.toString()}]`);

function arrayPartialReverse(a) {
    if (!a || !(a instanceof Array)) throw "Not an array";
    switch (a.length) {
        case 0:
        case 1:
            return a;
        default:
            var center = Math.floor(a.length / 2);
            for (let i=0; i < center; i++) {
                var x = a[i];
                a[i] = a[i + center+1];
                a[i + center+1] = x;
            }
    }
    return a;
}

console.log(`reverse array: [${arrayPartialReverse(array).toString()}]`);