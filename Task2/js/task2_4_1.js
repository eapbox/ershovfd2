// Ершов Александр
// Занятие №2 , задание4 вариант_1
//ECMAScript 5

readline = require('readline');

result = {
    firstName: '',
    userName: '',
    surName: '',
    age: '',
    isMale: ''
}

startApp();

//***************************************************************************************

function startApp() {
    var question = '';
    var typeQuestion = '';
    //****************************************
    if (!isNaN(Number(result.firstName))) {
        question = 'Введите фамилию: ';
        typeQuestion = 'firstName';
    } else if (!isNaN(Number(result.userName))) {
        question = 'Введите имя: ';
        typeQuestion = 'userName';
    } else if (!isNaN(Number(result.surName))) {
        question = 'Введите отчество: ';
        typeQuestion = 'surName';
    } else if (((result.age - (parseInt(result.age))) != 0) || (result.age <= 0) || (result.age > 140)) {
        question = 'Введите возраст (в годах): ';
        typeQuestion = 'age';
    } else if ((result.isMale != "true") && (result.isMale != "false")) {
        question = 'Вы мужчина? (true/false): ';
        typeQuestion = 'isMale';
    }

    //**************************************
    if (question !== '') {
        rl = prompt();
        rl.question(question, (answer) => {
            rl.close();
            switch (typeQuestion) {
                case 'firstName':
                    result.firstName = answer;
                    break;
                case 'userName':
                    result.userName = answer;
                    break;
                case 'surName':
                    result.surName = answer;
                    break;
                case 'age':
                    result.age = answer;
                    break;
                case 'isMale':
                    result.isMale = answer;
                    break;
            }
            startApp();
        });
    } else {
        printResult(result);
    }
}

//***************************************************************************************
function prompt() {
    return readline.createInterface({input: process.stdin, output: process.stdout});
}

function printResult(rez) {
    var worksheet = '\n' + 'Ваше ФИО: ' + rez.firstName + ' ' + rez.userName + ' ' + rez.surName + '\n';
    worksheet += 'Ваш возраст в годах: ' + rez.age + '\n';
    worksheet += 'Ваш возраст в днях: ' + (rez.age * 365) + '\n';
    worksheet += 'Через 5 лет вам будет: ' + (parseInt(rez.age) + 5) + '\n';
    if (rez.isMale === 'true')
        worksheet += 'Ваш пол: мужской\n';
    else
        worksheet += 'Ваш пол: женский\n';

    if (rez.age >= 60)
        worksheet += 'Вы на пенсии: да\n';
    else
        worksheet += 'Вы на пенсии: нет\n';

    console.log(worksheet);
}

