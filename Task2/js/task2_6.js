// Ершов Александр
// Занятие №2 , задание6
/*6. Сгенерировать массив из N случайных целых чисел.
     Поменять местами все элементы относительно середины массива по следующей схеме:
     Было [1,2,3,4,5,6,7,8,9], стало [4,3,2,1,5,9,8,7,6]. Создавать новые массивы нельзя.*/
'use strict';   //строгий режим js
var n = 9;
var array = [];

function randomDiap(N,M) {
    return Math.floor(
        Math.random()*(M-N+1))+N;
}

for (var i = 0; i < n; i++) {
    array[i] = randomDiap(1, 9);
}
console.log(`original array: [${array.toString()}]`);

function arrayPartialReverse(a) {
    if (!a || !(a instanceof Array)) throw "Not an array";
    switch (a.length) {
        case 0:
        case 1:
        case 2:
        case 3:
            // если в массиве до 3 элементов включительно,
            // то ничего делать не нужно
            return a;
        default:
            var arrLen = a.length;
            var center = Math.floor(arrLen / 2);
            var quoter = Math.floor(arrLen / 4);
            var add = arrLen % 2;

            for (var i=0; i < quoter; i++) {
                var x = a[i];
                a[i] = a[center - i - 1];
                a[center - i - 1] = x;

                x = a[center + i + add];
                a[center + i + add] = a[arrLen - i - 1];
                a[arrLen - i - 1] = x;
            }
    }
    return a;
}

console.log(`reverse array:  [${arrayPartialReverse(array).toString()}]`);
