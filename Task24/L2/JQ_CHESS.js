//JQ_CHESS_Ershov
'use strict';

$('.CHESS').contents().
    //черные квадраты в четной горизонтали и нечетной вертикали
    find('tr:nth-child(even) td:nth-child(odd)').attr('style','background-color: black;').end().
    //черные квадраты в нечетной горизонтали и четной вертикали
    find('tr:nth-child(odd) td:nth-child(even)').attr('style','background-color: black;').end().

    //узкие полоски
    find('tr:first-child td').attr('style','height: 20px; background-color: aquamarine;')
    .end().find('tr td:first-child').attr('style','width: 20px; background-color: aquamarine;')
    .end().find('tr:last-child td').attr('style','height: 20px; background-color: aquamarine;')
    .end().find('tr td:last-child').attr('style','width: 20px; background-color: aquamarine;')

    //перебиваем стили угловых кдавратов
    .end().find('tr:first-child td:first-child').attr('style','height: 20px; width: 20px; background-color: aquamarine;')
    .end().find('tr:first-child td:last-child').attr('style','height: 20px; width: 20px; background-color: aquamarine;')
    .end().find('tr:last-child td:first-child').attr('style','height: 20px; width: 20px; background-color: aquamarine;')
    .end().find('tr:last-child td:last-child').attr('style','height: 20px; width: 20px; background-color: aquamarine;')
;

