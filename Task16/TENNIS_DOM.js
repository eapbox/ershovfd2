//Task16 TENNIS_DOM
'use strict';
var field;  //игровое поле
var fieldParam = {width: "700px", height: "450px", color: "yellow", left: "30px", top: "60px"};
var start;  //кнопка старта

var ball;   //мячь
var ballParam = {radius: "30px", color: "red"};
var ballGradusMove;  //текущий угол движения мяча в градусах
var ballCurPos = {x: 0, y: 0};  //текущие координаты мяча
var tickTimBall = 0;   //таймер двтжения мяча
var ballSpeed = 3; //перемещение мяча в пикселах за один такт

var centreX;  //координаты центра поля
var centreY;

var score = {left:0,right:0};  //счет игры
var scoreElem;  //html элемент счета

var leftRocketParam = {width: "10px", height: "100px", color: "green", keyMoveUp: 16, keyMoveDown: 17};
var leftRocket;
var rightRocketParam = {width: "10px", height: "100px", color: "blue", keyMoveUp: 38, keyMoveDown: 40};
var rightRocket;
var rocketSpeed = 5;  //перемещение ракетки в пикселах за один такт
var tickTim = 20;     //частота смещения

var tickTimLeftRocket = 0;   //таймер двтжения левой ракетки
var tickTimRightRocket = 0;  //таймер двтжения правой ракетки

var body = document.body;

//событие после загрузки страницы--------------------------------
body.onload = function () {
    //отрисовываем игровое поле--------------------
    field = document.createElement('div');
    field.style.position = 'absolute';
    field.style.width = fieldParam.width;
    field.style.height = fieldParam.height;
    field.style.backgroundColor = fieldParam.color;
    field.style.border = 'solid 1px';
    field.style.top = fieldParam.top;
    field.style.left = fieldParam.left;
    field.style.boxSizing = 'border-box';
    field.style.zIndex = 1;
    body.appendChild(field);
    //--------------------------------------------------

    //отрисовка табло счета-----------------------------
    scoreElem = document.createElement('div');
    scoreElem.style.position = 'absolute';
    scoreElem.style.textAlign = 'center';
    scoreElem.style.width = "100px";
    scoreElem.style.height = "40px";
    scoreElem.style.fontSize = "32px";
    scoreElem.style.left = parseInt(fieldParam.left) + parseInt(fieldParam.width)/2 - parseInt(scoreElem.style.width)/2 + "px";
    scoreElem.style.top = parseInt(fieldParam.top) - parseInt(scoreElem.style.height) + "px";
    scoreElem.innerHTML = `${score.left}:${score.right}`;
    body.appendChild(scoreElem);
    //--------------------------------------------------

    //вычисляем центра поля-----------------------------
    centreX = parseInt(fieldParam.left) + parseInt(fieldParam.width) / 2;
    centreY = parseInt(fieldParam.top) + parseInt(fieldParam.height) / 2;
    //--------------------------------------------------

    //отрисовываем центр поля---------------------------
    var crossX = document.createElement('div');
    crossX.style.position = 'absolute';
    crossX.style.height = '1px';
    crossX.style.width = '60px';
    crossX.style.backgroundColor = "red";
    crossX.style.left = centreX - parseInt(crossX.style.width) / 2 + "px";
    crossX.style.top = centreY + "px";
    crossX.style.zIndex = 20;
    body.appendChild(crossX);

    var crossY = document.createElement('div');
    crossY.style.position = 'absolute';
    crossY.style.height = '60px';
    crossY.style.width = '1px';
    crossY.style.backgroundColor = "red";
    crossY.style.left = centreX + "px";
    crossY.style.top = centreY - parseInt(crossY.style.height) / 2 + "px";
    crossY.style.zIndex = 20;
    body.appendChild(crossY);
    //--------------------------------------------------

    //отрисовываем ракетки------------------------------
    leftRocket = document.createElement('div');
    leftRocket.style.position = 'absolute';
    leftRocket.style.width = leftRocketParam.width;
    leftRocket.style.height = leftRocketParam.height;
    leftRocket.style.backgroundColor = leftRocketParam.color;
    leftRocket.style.left = fieldParam.left;
    leftRocket.style.top = (parseInt(fieldParam.top) +
        parseInt(fieldParam.height) / 2 -
        parseInt(leftRocketParam.height) / 2) + "px";
    leftRocket.style.zIndex = 10;
    body.appendChild(leftRocket);

    rightRocket = document.createElement('div');
    rightRocket.style.position = 'absolute';
    rightRocket.style.width = rightRocketParam.width;
    rightRocket.style.height = rightRocketParam.height;
    rightRocket.style.backgroundColor = rightRocketParam.color;
    rightRocket.style.left = (parseInt(fieldParam.left) +
        parseInt(fieldParam.width) -
        parseInt(rightRocketParam.width)) + "px";
    rightRocket.style.top = (parseInt(fieldParam.top) +
        parseInt(fieldParam.height) / 2 -
        parseInt(rightRocketParam.height) / 2) + "px";
    rightRocket.style.zIndex = 10;
    body.appendChild(rightRocket);
    //---------------------------------------------------

    //создаем мячь---------------------------------------
    ball = document.createElement('div');
    ball.style.position = 'absolute';
    ball.style.height = ballParam.radius;
    ball.style.width = ballParam.radius;
    ball.style.borderRadius = '50%';
    ball.style.backgroundColor = ballParam.color;
    ball.style.top = centreY - parseInt(ball.style.height) / 2 + "px";
    ball.style.left = centreX - parseInt(ball.style.width) / 2 + "px";
    ball.style.zIndex = 30;
    body.appendChild(ball);
    //---------------------------------------------------

    //отрисовываем кнопку старта-------------------------
    start = document.createElement('input');
    start.setAttribute('type', 'button');
    start.setAttribute('value', 'Старт!');
    start.style.fontSize = '28px';
    start.style.position = 'absolute';
    start.style.left = fieldParam.left;
    body.appendChild(start);
    start.style.top = parseInt(fieldParam.top) - parseInt(start.style.height) + "px";
    start.addEventListener("click", gameStart); //зупуск мяча
    //---------------------------------------------------

    //подписываемся на нажатие клавиш-------------
    window.addEventListener("keydown", keyDown);
    window.addEventListener("keyup", keyUp);
    //--------------------------------------------

} //основной поток после загрузки страницы
//---------------------------------------------------------------------

//начинаем игру - запускаем мячь---------------------------------------
function gameStart() {
    if (tickTimBall) {
        clearInterval(tickTimBall);
        tickTimBall = 0;
    }

    ballCurPos.x = centreX - parseInt(ball.style.width) / 2;
    ballCurPos.y = centreY - parseInt(ball.style.height) / 2;

    ball.style.top = ballCurPos.y + "px";
    ball.style.left = ballCurPos.x + "px";
    ballGradusMove = randomDiap(0,360); //первоначальный угол движения мяча
    ballSpeed = 3;

    tickTimBall = setInterval(moveBall, tickTim);
}

function moveBall() {
    //координаты возможных точек касания----------------------------
    let touch = {top: {x: 0, y: 0}, right: {x: 0, y: 0}, bottom: {x: 0, y: 0}, left: {x: 0, y: 0}};

    //вычисляем прогназируемую следующую координату (верхний левый угол мяча)
    let nextX = ballCurPos.x + ballSpeed * Math.cos(gradus(ballGradusMove));
    let nextY = ballCurPos.y + ballSpeed * Math.sin(gradus(ballGradusMove));

    touch.top.x = nextX + parseInt(ball.style.width) / 2;
    touch.top.y = nextY;
    touch.right.x = nextX + parseInt(ball.style.width);
    touch.right.y = nextY + parseInt(ball.style.height) / 2;
    touch.bottom.x = nextX + parseInt(ball.style.width) / 2;
    touch.bottom.y = nextY + parseInt(ball.style.height);
    touch.left.x = nextX;
    touch.left.y = nextY + parseInt(ball.style.height) / 2;
    //---------------------------------------------------------------

    if (touch.top.y <= parseInt(fieldParam.top)) {
        ballGradusMove = 360 - ballGradusMove;
    } else if (touch.bottom.y >= (parseInt(fieldParam.top) + parseInt(fieldParam.height))) {
        ballGradusMove = 360 - ballGradusMove;
    } else if ( (touch.right.x >= parseInt(rightRocket.style.left)) && (touch.right.y >= parseInt(rightRocket.style.top)) && (touch.right.y <= parseInt(rightRocket.style.top) +parseInt(rightRocket.style.height))) {
        //отбили мячь правой ракеткой
        ballGradusMove = 180-ballGradusMove;
        ++ballSpeed;
    } else if ( (touch.left.x <= parseInt(leftRocket.style.left)+parseInt(leftRocket.style.width)) && (touch.left.y >= parseInt(leftRocket.style.top)) && (touch.left.y <= parseInt(leftRocket.style.top) +parseInt(leftRocket.style.height))) {
        //отбили мячь левой ракеткой
        ballGradusMove = 180-ballGradusMove;
        ++ballSpeed;
    } else if (touch.right.x >= (parseInt(fieldParam.left) + parseInt(fieldParam.width))) {
        //гол в правые ворота
        clearInterval(tickTimBall);
        tickTimBall = 0;
        ++score.left;
        scoreElem.innerHTML = `${score.left}:${score.right}`;
        return;
    } else if (touch.left.x <= parseInt(fieldParam.left)) {
        //гол в левые ворота
        clearInterval(tickTimBall);
        tickTimBall = 0;
        ++score.right;
        scoreElem.innerHTML = `${score.left}:${score.right}`;
        return;
    }

    ballCurPos.x = nextX;
    ballCurPos.y = nextY;
    ball.style.top = ballCurPos.y + "px";
    ball.style.left = ballCurPos.x + "px";
}
//---------------------------------------------------------------------

//обрабатываем нажатую клавишу (запуск таймера движения ракетки)-------
function keyDown(EL) {
    switch (EL.keyCode) {
        case leftRocketParam.keyMoveUp:
            if (!tickTimLeftRocket) {
                tickTimLeftRocket = setInterval(moveLeftRocketUp, tickTim);
            }
            break;
        case leftRocketParam.keyMoveDown:
            if (!tickTimLeftRocket) {
                tickTimLeftRocket = setInterval(moveLeftRocketDown, tickTim);
            }
            break;
        case rightRocketParam.keyMoveUp:
            if (!tickTimRightRocket) {
                tickTimRightRocket = setInterval(moveRightRocketUp, tickTim);
            }
            break;
        case rightRocketParam.keyMoveDown:
            if (!tickTimRightRocket) {
                tickTimRightRocket = setInterval(moveRightRocketDown, tickTim);
            }
            break;
    }
}
//-----------------------------------------------------------------------

//обрабатываем отжатую клавишу (остановка таймера движения ракетки)-------
function keyUp(EL) {
    switch (EL.keyCode) {
        case leftRocketParam.keyMoveUp:
            clearInterval(tickTimLeftRocket);
            tickTimLeftRocket = 0;
            break;
        case leftRocketParam.keyMoveDown:
            clearInterval(tickTimLeftRocket);
            tickTimLeftRocket = 0;
            break;
        case rightRocketParam.keyMoveUp:
            clearInterval(tickTimRightRocket);
            tickTimRightRocket = 0;
            break;
        case rightRocketParam.keyMoveDown:
            clearInterval(tickTimRightRocket);
            tickTimRightRocket = 0;
            break;
    }
}
//-----------------------------------------------------------------------

//перемещение ракеток----------------------------------------------------
function moveLeftRocketUp() {
    moveRocket(leftRocket, -rocketSpeed);
}

function moveLeftRocketDown() {
    moveRocket(leftRocket, rocketSpeed);
}

function moveRightRocketUp() {
    moveRocket(rightRocket, -rocketSpeed);
}

function moveRightRocketDown() {
    moveRocket(rightRocket, rocketSpeed);
}

function moveRocket(rocket, move) {
    var nextTop = parseInt(rocket.style.top) + move; //следующее приращение
    if (nextTop >= parseInt(fieldParam.top)) {
        if (nextTop <= (parseInt(fieldParam.top) + parseInt(fieldParam.height) - parseInt(rocket.style.height)))
            rocket.style.top = nextTop + "px";
    }
}
//-----------------------------------------------------------------------------------

//перевод градуса в радианы-------------
var gradus = function toRadians(angle) {
    return angle * (Math.PI / 180);
}
//--------------------------------------
//случайное число в диапазоне n-m
function randomDiap(n, m) {
    return Math.floor(Math.random() * (m - n + 1)) + n;
}
//--------------------------------------



