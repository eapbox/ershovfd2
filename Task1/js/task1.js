'use strict';   //строгий режим js
// https://eapbox@bitbucket.org/eapbox/ershovfd2.git

var firstName = getName('Введите фамилию: ');
var userName = getName('Введите имя: ');
var surName = getName('Введите отчество: ');
var age = getAge();
var isMale = confirm('Вы мужчина?');

var worksheet = 'Ваше ФИО: ' + firstName + ' ' + userName + ' ' + surName + '\n';
worksheet += 'Ваш возраст в годах: ' + age + '\n';
worksheet += 'Ваш возраст в днях: ' + (age*365) + '\n';
worksheet += 'Через 5 лет вам будет: ' + (parseInt(age)+5) + '\n';

if (isMale)
    worksheet += 'Ваш пол: мужской\n';
else
    worksheet += 'Ваш пол: женский\n';

if (age >= 60)
    worksheet += 'Вы на пенсии: да\n';
else
    worksheet += 'Вы на пенсии: нет\n';

alert(worksheet);

// Вставка результата на страницу------------
var parentElem = document.body;
parentElem.innerHTML = `<pre>${worksheet}</pre>`;
//-------------------------------------------
//*****************************************************

function getAge() {
    while (true) {
        var age = prompt('Введите возраст (в годах): ');
        if (!isNaN(Number(age))) {
            if (((age - (parseInt(age))) == 0) && (age > 0) && (age < 150))
                break;
        }
    }
    return age;
}

function getName(question) {
    while (true) {
        var str = prompt(question);
        if (isNaN(Number(str))) break;
    }
    return str;
}

