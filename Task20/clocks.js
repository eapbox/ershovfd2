'use strict';

//3. передаем элемент, где будем отрисовывать часы-------------
class ApplicationView { // Shell
    constructor(element) {
        this.element = element;
    }

    //готовим элементы/площадки под размещение часов-----------------------------------------
    render() {
        var fieldStyle = 'float: left; width: 30%; background-color: #CAF2FF; margin: 1%';
        var butnStyle = 'width: 70px; height:30px; margin: 1%';

        //1--------------------------------------------------
        var div = this.div1 = document.createElement('div');
        div.style = fieldStyle;
        this.element.appendChild(div);
        var butn = document.createElement('button');
        butn.textContent = "Стоп";
        butn.className = "stop";
        butn.style = butnStyle;
        div.appendChild(butn);
        butn = document.createElement('button');
        butn.textContent = "Старт";
        butn.className = "start";
        butn.style = butnStyle;
        div.appendChild(butn);
        var curZone = document.createElement('span');
        curZone.className = "curZone";
        div.appendChild(curZone);

        //2-------------------------------------------------
        div = this.div2 = document.createElement('div');
        div.style = fieldStyle;
        this.element.appendChild(div);
        butn = document.createElement('button');
        butn.textContent = "Стоп";
        butn.className = "stop";
        butn.style = butnStyle;
        div.appendChild(butn);
        butn = document.createElement('button');
        butn.textContent = "Старт";
        butn.className = "start";
        butn.style = butnStyle;
        div.appendChild(butn);
        curZone = document.createElement('span');
        curZone.className = "curZone";
        div.appendChild(curZone);

        //3-------------------------------------------------
        div = this.div3 = document.createElement('div');
        div.style = fieldStyle;
        this.element.appendChild(div);
        butn = document.createElement('button');
        butn.textContent = "Стоп";
        butn.className = "stop";
        butn.style = butnStyle;
        div.appendChild(butn);
        butn = document.createElement('button');
        butn.textContent = "Старт";
        butn.className = "start";
        butn.style = butnStyle;
        div.appendChild(butn);
        curZone = document.createElement('span');
        curZone.className = "curZone";
        div.appendChild(curZone);

        //4-------------------------------------------------
        div = this.div4 = document.createElement('div');
        div.style = fieldStyle;
        this.element.appendChild(div);
        butn = document.createElement('button');
        butn.textContent = "Стоп";
        butn.className = "stop";
        butn.style = butnStyle;
        div.appendChild(butn);
        butn = document.createElement('button');
        butn.textContent = "Старт";
        butn.className = "start";
        butn.style = butnStyle;
        div.appendChild(butn);
        curZone = document.createElement('span');
        curZone.className = "curZone";
        div.appendChild(curZone);

        //5-------------------------------------------------
        div = this.div5 = document.createElement('div');
        div.style = fieldStyle;
        this.element.appendChild(div);
        butn = document.createElement('button');
        butn.textContent = "Стоп";
        butn.className = "stop";
        butn.style = butnStyle;
        div.appendChild(butn);
        butn = document.createElement('button');
        butn.textContent = "Старт";
        butn.className = "start";
        butn.style = butnStyle;
        div.appendChild(butn);
        curZone = document.createElement('span');
        curZone.className = "curZone";
        div.appendChild(curZone);

        //6-------------------------------------------------
        div = this.div6 = document.createElement('div');
        div.style = fieldStyle;
        this.element.appendChild(div);
        butn = document.createElement('button');
        butn.textContent = "Стоп";
        butn.className = "stop";
        butn.style = butnStyle;
        div.appendChild(butn);
        butn = document.createElement('button');
        butn.textContent = "Старт";
        butn.className = "start";
        butn.style = butnStyle;
        div.appendChild(butn);
        curZone = document.createElement('span');
        curZone.className = "curZone";
        div.appendChild(curZone);
        //--------------------------------------------------
    }
}

//------------------------------------------------------------

//2. Это первое с чего стартует наше приложение.------------------------------------------
class ApplicationController {
    constructor(applicationView) {
        this.applicationView = applicationView;
        applicationView.render(); //3 - отрисовываем "площадки" под часы
        var pubSubService = window.pubSubService = new PubSubService();

        //4 - стартуем часы
        new ClockController(
            new ClockModel("GMT-5",pubSubService, "clock1"),               //5- запускаем расчет времени
            new ClockViewDiv(applicationView.div1),
            pubSubService);  //6- отрисовка графики часов

        new ClockController(
            new ClockModel("GMT+0",pubSubService, "clock2"),
            new ClockViewDiv(applicationView.div2),
            pubSubService);

        new ClockController(new ClockModel("GMT+1",pubSubService, "clock3"),
            new ClockViewSvg(applicationView.div3),
            pubSubService);

        new ClockController(
            new ClockModel("GMT+3",pubSubService, "clock4"),
            new ClockViewSvg(applicationView.div4),
            pubSubService);

        new ClockController(
            new ClockModel("GMT+9",pubSubService, "clock5"),
            new ClockViewDiv(applicationView.div5),
            pubSubService);

        new ClockController(
            new ClockModel("GMT+10",pubSubService, "clock6"),
            new ClockViewDiv(applicationView.div6),
            pubSubService);
    }
}
//--------------------------------------------------------------------------------------------------

//Отрисовка часов на Div-------------------------------------------------------
class ClockViewDiv {
    constructor(element) {
        this.element = element;
    }

    render() {
        //циферблат----------------------------------
        var base = document.createElement('div');
        this.element.appendChild(base);
        base.style = 'position: relative; width: 300px; height: 300px; background: #7cb5dc; border-radius: 50%; box-shadow: 0 0 20px gray; margin: 0 auto;';

        //центр циферблата
        var baseCenterX = parseInt(getComputedStyle(base).width) / 2;
        var baseCenterY = parseInt(getComputedStyle(base).height) / 2;

        //цифры------------------------------
        var numSize = baseCenterX / 3.5;
        var fontSize = baseCenterX / 5;
        var numSignCorrection = numSize / 2;

        for (var i = 1; i <= 12; i++) {
            var num = document.createElement('div');
            base.appendChild(num);

            var angle = Math.PI / 6 * (9 - i);

            num.style = 'position: absolute; background: green; border-radius: 50%; display: ' +
                'flex; align-items: center; justify-content: center; color: white; font-family: arial;';
            num.style.width = numSize + 'px';
            num.style.height = numSize + 'px';
            num.style.fontSize = fontSize + 'px';
            num.style.left = baseCenterX - Math.cos(angle) * baseCenterX * 0.8 - numSignCorrection + 'px';
            num.style.top = baseCenterY + Math.sin(angle) * baseCenterY * 0.8 - numSignCorrection + 'px';
            num.innerHTML = '<span>' + i + '</span>';
        }

        //стрелка час
        var hour = this.hour = document.createElement('div');
        base.appendChild(hour);
        hour.style = 'position: absolute; height: 10px; background: black; border-radius: 5px;';
        hour.style.width = baseCenterX / 2.5 + 'px';
        hour.style.left = baseCenterX - 10 + 'px';
        hour.style.top = baseCenterY - 5 + 'px';
        hour.style.transformOrigin = '10px';


        //стрелка минуты
        var min = this.min = document.createElement('div');
        base.appendChild(min);
        min.style = 'position: absolute; height: 5px; background: black; border-radius: 2px;';
        min.style.width = baseCenterX / 1.6 + 'px';
        min.style.left = baseCenterX - 15 + 'px';
        min.style.top = baseCenterY - 2 + 'px';
        min.style.transformOrigin = '15px';


        //стрелка секунды
        var sec = this.sec = document.createElement('div');
        base.appendChild(sec);
        sec.style = 'position: absolute; height: 3px; background: black; border-radius: 2px;';
        sec.style.width = baseCenterX / 1.2 + 'px';
        sec.style.left = baseCenterX - 20 + 'px';
        sec.style.top = baseCenterY - 1 + 'px';
        sec.style.transformOrigin = '20px';

        //электр часы
        var electronicWatch = this.electronicWatch = document.createElement('div');
        base.appendChild(electronicWatch);
        electronicWatch.style = 'position: absolute; top: 25%; width: 100%; text-align: center; font-family: arial; font-size: 32px';
    }

    update(model) {
        this.hour.style.transform = 'rotate(' + model.rotateHour + 'deg)';
        this.min.style.transform = 'rotate(' + model.rotateMin + 'deg)';
        this.sec.style.transform = 'rotate(' + model.rotateSec + 'deg)';
        this.electronicWatch.innerHTML = model.electronicWatchText;
    }
}
//-------------------------------------------------------------------------

//Отрисовка часов на Svg-------------------------------------------------------
class ClockViewSvg {
    constructor(element) {
        this.element = element;
    }

    render() {
        let rad = function toRadians(angle) {
            return angle * (Math.PI / 180);
        };

        //создаем сцену
        let SVGElem = document.createElementNS("http://www.w3.org/2000/svg","svg");
        this.element.appendChild(SVGElem);
        SVGElem.style = 'position: relative; width: 300px; height: 300px; z-index: 2; margin: 0 auto;';
        let scene=getComputedStyle(SVGElem);
        let width = scene.width;
        let height = scene.height;
        let centerX = this.centerX = parseInt(scene.left)  + (parseInt( width)/ 2);
        let centerY = this.centerY = parseInt(scene.top)  + (parseInt(height)  / 2);
        let clockR = parseInt(width)/2;
        let digR = parseInt(0.15 * clockR);

        //циферблат----------------------------------
        let circle = document.createElementNS("http://www.w3.org/2000/svg", 'circle');
        circle.setAttribute("stroke", "green");
        circle.setAttribute("fill", "yellow");
        circle.setAttribute("r", clockR);
        circle.setAttribute("cx", centerX);
        circle.setAttribute("cy", centerY);
        SVGElem.appendChild(circle);
        //-------------------------------------------

        //формируем цифры часов------------------------------------------
        for (let i = 0; i < 12; i++) {
            let x, y;  //координаты центра текущей цифры
            x = centerX + (clockR - 1.5 * digR) * Math.cos(rad(30) * i - rad(60));
            y = centerY + (clockR - 1.5 * digR) * Math.sin(rad(30) * i - rad(60));

            let dig = document.createElementNS("http://www.w3.org/2000/svg", 'circle');
            dig.setAttribute("stroke", "green");
            dig.setAttribute("fill", "#eefeff");
            dig.setAttribute("r", digR);
            dig.setAttribute("cx", x);
            dig.setAttribute("cy", y);
            SVGElem.appendChild(dig);

            //вставляем цифру---------------------------
            let txt = document.createElementNS("http://www.w3.org/2000/svg", 'text');
            txt.setAttribute("x", x);
            txt.setAttribute("y", y + (32 / 3));
            txt.setAttribute("text-anchor", "middle");
            txt.setAttribute("font-size", 32);
            txt.style.fill = "dodgerblue";
            txt.textContent = i + 1;
            SVGElem.appendChild(txt);
            //--------------------------------------
        }
        //---------------------------------------------------------------

        //создаем элементы стрелок часов---------------------------------
        let lineHour = this.hour = document.createElementNS("http://www.w3.org/2000/svg", 'line');
        lineHour.setAttribute("x1", centerX);
        lineHour.setAttribute("y1", centerY + 0.1 * clockR);
        lineHour.setAttribute("x2", centerX);
        lineHour.setAttribute("y2", centerY - 0.45 * clockR);
        lineHour.setAttribute("stroke", "green");
        lineHour.setAttribute("stroke-width", "6");
        SVGElem.appendChild(lineHour);

        let lineMin = this.min = document.createElementNS("http://www.w3.org/2000/svg", 'line');
        lineMin.setAttribute("x1", centerX);
        lineMin.setAttribute("y1", centerY + 0.1 * clockR);
        lineMin.setAttribute("x2", centerX);
        lineMin.setAttribute("y2", centerY - 0.65 * clockR);
        lineMin.setAttribute("stroke", "green");
        lineMin.setAttribute("stroke-width", "4");
        SVGElem.appendChild(lineMin);

        let lineSec = this.sec = document.createElementNS("http://www.w3.org/2000/svg", 'line');
        lineSec.setAttribute("x1", centerX);
        lineSec.setAttribute("y1", centerY + 0.1 * clockR);
        lineSec.setAttribute("x2", centerX);
        lineSec.setAttribute("y2", centerY - 0.9 * clockR);
        lineSec.setAttribute("stroke", "green");
        lineSec.setAttribute("stroke-width", "1");
        SVGElem.appendChild(lineSec);
        //---------------------------------------------------------------

        //создаем цифровые часы------------------------------------------
        let digitalClock = this.electronicWatch = document.createElementNS("http://www.w3.org/2000/svg", 'text');
        digitalClock.setAttribute("x", centerX);
        digitalClock.setAttribute("y", centerY - 0.4*centerY);
        digitalClock.setAttribute("text-anchor", "middle");
        digitalClock.setAttribute("font-size", 20);
        digitalClock.style.fill = "dodgerblue";
        SVGElem.appendChild(digitalClock);
        //-----------------------------------------------------------
}

    update(model) {
        this.hour.setAttribute("transform",`rotate(${model.rotateHour} ${this.centerX} ${this.centerY})`);
        this.min.setAttribute("transform",`rotate(${model.rotateMin} ${this.centerX} ${this.centerY})`);
        this.sec.setAttribute("transform",`rotate(${model.rotateSec} ${this.centerX} ${this.centerY})`);

        this.electronicWatch.innerHTML = model.electronicWatchText;
    }
}  //svg
//-------------------------------------------------------------------------

//5 - модель часов - счет времени------------------------------------------
class ClockModel {
    constructor(timeZone, pubSubService, modelName) {
        this.timeZone = timeZone;
        this.pubSubService = pubSubService;
        this.modelName = modelName;
        this.reg = new RegExp(`GMT[+-][0-9]{1,2}`);
        this.regRepl = new RegExp(`GMT`);

        //часы-------------------
        this.time = null;
        this.seconds = null;
        this.minutes = null;
        this.hours = null;
        this.rotateSec = null;
        this.rotateMin = null;
        this.rotateHour = null;
        this.timeout = null;
        //----------------------
    }

    //Устанавливаем требуемый часовой пояс----------------------
    getCurentDate() {
        var dt = new Date();
        var TimeZoneOffsetHours = dt.getTimezoneOffset() / 60;
        var utc = dt.getTime();

        if ((this.timeZone.search(this.reg)) >= 0) {
            let curOffset = parseInt(this.timeZone.replace(this.regRepl, ''));
            dt.setTime(utc + 3600000 * (TimeZoneOffsetHours + curOffset));
            return dt;
        }
        //если не корректно передан часовой пояс, то считаем время по Гринвичу
        dt.setTime(utc + 3600000 * (TimeZoneOffsetHours));
        return dt;
    }
    //-----------------------------------------------------------

    clock() {
        //this.time = new Date(Date.now()); // TODO: использовать this.timeZone !
        this.time = this.getCurentDate();

        this.seconds = this.time.getSeconds();
        this.minutes = this.time.getMinutes();
        this.hours = this.time.getHours();
        this.rotateSec = this.seconds * 6 - 90;
        this.rotateMin = this.minutes * 6 - 90 + this.seconds / 12;
        this.rotateHour = this.time.getHours() * 30 - 90 + this.minutes / 2;

        function addZero(t) {
            if (t < 10) {
                t = '0' + t;
            }
            return t;
        }

        this.minutes = addZero(this.minutes);
        this.seconds = addZero(this.seconds);
        this.electronicWatchText = this.hours + ':' + this.minutes + ':' + this.seconds;

        setTimeout(this.clock.bind(this), 1000);

        this.pubSubService.pub(this.modelName + 'updated');
    }
}

class PubSubService {
    constructor() {
        this.listeners = {};
    }

    sub(eventName, listener) {
        if (typeof (listener) !== 'function') throw "Only functions can be listerners";

        if (eventName in this.listeners) {
            if (this.listeners[eventName].indexOf(listener) == -1) {
                this.listeners[eventName].push(listener);
            }
        }
        else {
            this.listeners[eventName] = [listener];
        }
    }

    pub(eventName) {
        let arrayOfListeners = this.listeners[eventName];
        if (arrayOfListeners) {
            for (let listener of arrayOfListeners) {
                listener();
            }
        }
    }

    delete(eventName, listener) {
        let arrayOfListeners = this.listeners[eventName];
        if (arrayOfListeners) {
            const index = arrayOfListeners.indexOf(listener);
            if (index > -1) {
                arrayOfListeners.splice(index, 1);
            }
        }
    }
}

//4. ----------------------------------------------------------
class ClockController {
    constructor(model, view, pubSubService) {
        this.model = model;
        this.view = view;
        this.view.render();
        //this.model.updated = () => this.view.update(this.model);
        const listener = () => this.view.update(this.model);

        pubSubService.sub(this.model.modelName + 'updated', listener);

        var btn = view.element.querySelector('.stop');
        if (btn) {
            btn.onclick = e => pubSubService.delete(this.model.modelName + 'updated', listener);
        }
        btn = view.element.querySelector('.start');
        if (btn) {
            btn.onclick = e => pubSubService.sub(this.model.modelName + 'updated', listener);
            pubSubService.sub(this.model.modelName + 'updated', () => console.log("Второй обработчик вызван"));
        }

        this.model.clock();
    }
}
//--------------------------------------------------------------

//1. start SPA---------------------------------------------------
var clocks = new ApplicationController(new ApplicationView(document.body));

//---------------------------------------------------------------