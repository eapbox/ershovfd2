'use strict';

//**********************************************************
function HashStorage(name) {
    var self = this;
    self.name = name;
    self.reg = new RegExp(`${self.name}_`,'g');

    self.addValue = function (key, value) {
        localStorage.setItem(`${self.name}_${key}`, JSON.stringify(value));
    }
    self.getValue = function (key) {
        return JSON.parse(localStorage[`${self.name}_${key}`]);
    }
    self.deleteValue = function (key) {
        if (`${self.name}_${key}` in localStorage) {
            localStorage.removeItem(`${self.name}_${key}`);
            return true;
        }
        return false;
    }
    self.getKeys = function () {
        var keys = [];
        for (var key in localStorage) {
            if (key.search(self.reg) >= 0) {
                keys.push(key.replace(self.reg, ''));
            }
        }
        return keys;
    }
}
//**********************************************************

var drinkStorage = new HashStorage('drink');
var dishStorage = new HashStorage('dish');
var messageDrink = document.getElementById('message1');
var messageDish = document.getElementById('message2');

//Drinks------------------------------------------------------
function createDrink() {
    let name = prompt('Введите название напитка: ');
    let isAlch = confirm('Алкогольный?');
    let recipe = prompt('Рецепт приготовления: ');

    drinkStorage.addValue(name, {isAlch:isAlch.toString(), recipe: recipe.toString()});
}

function getInfoDrink() {
    let name = prompt('Введите название напитка: ');
    let drink = drinkStorage.getValue(name);

    if (drink) {
        messageDrink.innerHTML = `напиток: ${name}&#13;&#10;алкогольный: ${drink.isAlch}&#13;&#10;рецепт приготовления:&#13;&#10;${drink.recipe}`;
        return;
    }
    messageDrink.innerHTML = `напитка: ${name} - нет!`;
}

function deleteDrink() {
    let name = prompt('Введите название напитка: ');
    if (drinkStorage.deleteValue(name)) {
        messageDrink.innerHTML = `напиток: ${name} - успешно удален!`;
        return;
    }
    messageDrink.innerHTML = `напитка: ${name} - нет!`;
}

function listDrinks() {
    let drinks = drinkStorage.getKeys();
    let list = '';
    for (var el of drinks) {
        list +=el + '&#13;&#10;';
    }
    messageDrink.innerHTML = list;
}
//-------------------------------------------------------------------------


//Dishs------------------------------------------------------
function createDish() {
    let name = prompt('Введите название блюда: ');
    let isMeet = confirm('Мясное?');
    let recipe = prompt('Рецепт приготовления: ');

    dishStorage.addValue(name, {isMeet:isMeet.toString(), recipe: recipe.toString()});
}

function getInfoDish() {
    let name = prompt('Введите название блюда: ');
    let dish = dishStorage.getValue(name);

    if (dish) {
        messageDish.innerHTML = `блюдо: ${name}&#13;&#10;мясное: ${dish.isMeet}&#13;&#10;рецепт приготовления:&#13;&#10;${dish.recipe}`;
        return;
    }
    messageDish.innerHTML = `блюда: ${name} - нет!`;
}

function deleteDish() {
    let name = prompt('Введите название блюда: ');
    if (dishStorage.deleteValue(name)) {
        messageDish.innerHTML = `блюдо: ${name} - успешно удалено!`;
        return;
    }
    messageDish.innerHTML = `блюда: ${name} - нет!`;
}

function listDishs() {
    let dishs = dishStorage.getKeys();
    let list = '';
    for (var el of dishs) {
        list +=el + '&#13;&#10;';
    }
    messageDish.innerHTML = list;
}
//-------------------------------------------------------------------------




