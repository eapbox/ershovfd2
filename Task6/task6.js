// Ершов Александр
// Task6
/*
Написать «чистую» («молчаливую») функцию для эффективного подсчёта количества русских гласных букв в строке.
Спросить у пользователя строку. Вывести в консоль количество русских гласных букв в ней.
*/
'use strict';
var vowels = {а: 'а', о: 'о', э: 'э', и: 'и', у: 'у', ы: 'ы', е: 'е', ё: 'ё', ю: 'ю', я: 'я'};

let str = prompt('Введите строку');

function getCountVowels(text) {
    var countVowels = 0;
    for (var symbol of text) {
        if (symbol.toLowerCase() in vowels)
            countVowels++;
    }
    return countVowels;
}

function getCountVowelsRecurs(text, start = 0, cntVow = 0) {
    var countVowels = 0;
    var array = text.split("");

    if (array[start].toLowerCase() in vowels)
        cntVow++;
    start++;

    if (start >= array.length){
        console.log(cntVow);
        return cntVow;
    }

    getCountVowelsRecurs(text,start,cntVow);
}

function showCountVowels(str) {
    var countVowels = getCountVowels(str);
    // var countVowels = getCountVowelsRecurs(str);
    alert(`гласных букв: ${countVowels}`);
}

showCountVowels(str);
