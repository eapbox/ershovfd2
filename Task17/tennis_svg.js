'use strict';
var field;  //игровое поле
var fieldParam = {width: 700, height: 450, color: "yellow", left: 30, top: 60};

var centreX;  //координаты центра поля
var centreY;
var score = {left: 0, right: 0};  //счет игры
var scoreElem;  //элемент счета

var leftRocketParam = {width: 15, height: 100, color: "green", keyMoveUp: 16, keyMoveDown: 17};
var leftRocket;
var rightRocketParam = {width: 15, height: 100, color: "blue", keyMoveUp: 38, keyMoveDown: 40};
var rightRocket;
var rocketSpeed = 3;  //перемещение ракетки в пикселах за один такт
var tickTim = 20;     //частота смещения

var tickTimLeftRocket = 0;   //таймер двтжения левой ракетки
var tickTimRightRocket = 0;  //таймер двтжения правой ракетки

var ball;   //мячь
var ballParam = {radius: 15, color: "red"};
var ballGradusMove;  //текущий угол движения мяча в градусах
var ballCurPos = {x: 0, y: 0};  //текущие координаты мяча
var tickTimBall = 0;   //таймер двтжения мяча
var ballSpeed = 1; //перемещение мяча в пикселах за один такт


//событие после загрузки страницы-----------------------------------------
document.body.onload = function () {
    var SVGElem = document.getElementById("mainSvg");

    //отрисовываем игровое поле-------------------------
    field = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
    field.setAttribute("stroke", "green");
    field.setAttribute("fill", fieldParam.color);
    field.setAttribute("x", fieldParam.left);
    field.setAttribute("y", fieldParam.top);
    field.setAttribute("width", fieldParam.width);
    field.setAttribute("height", fieldParam.height);
    SVGElem.appendChild(field);
    //--------------------------------------------------

    //отрисовка табло счета-----------------------------
    scoreElem = document.createElementNS("http://www.w3.org/2000/svg", 'text');
    scoreElem.setAttribute("x", fieldParam.left + (fieldParam.width / 2));
    scoreElem.setAttribute("y", 0.8 * fieldParam.top);
    scoreElem.setAttribute("text-anchor", "middle");
    scoreElem.setAttribute("font-size", 32);
    scoreElem.setAttribute("fill", "dodgerblue");
    scoreElem.textContent = `${score.left}:${score.right}`;
    SVGElem.appendChild(scoreElem);
    //--------------------------------------------------

    //вычисляем центра поля-----------------------------
    centreX = fieldParam.left + fieldParam.width / 2;
    centreY = fieldParam.top + fieldParam.height / 2;
    //--------------------------------------------------

    //отрисовываем центр поля---------------------------
    var crossX = document.createElementNS("http://www.w3.org/2000/svg", 'line');
    crossX.setAttribute("x1", centreX - 20);
    crossX.setAttribute("y1", centreY);
    crossX.setAttribute("x2", centreX + 20);
    crossX.setAttribute("y2", centreY);
    crossX.setAttribute("stroke", "red");
    crossX.setAttribute("stroke-width", "1");
    SVGElem.appendChild(crossX);

    var crossY = document.createElementNS("http://www.w3.org/2000/svg", 'line');
    crossY.setAttribute("x1", centreX);
    crossY.setAttribute("y1", centreY - 20);
    crossY.setAttribute("x2", centreX);
    crossY.setAttribute("y2", centreY + 20);
    crossY.setAttribute("stroke", "red");
    crossY.setAttribute("stroke-width", "1");
    SVGElem.appendChild(crossY);
    //--------------------------------------------------

    //отрисовываем ракетки------------------------------
    leftRocket = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
    leftRocket.setAttribute("fill", leftRocketParam.color);
    leftRocket.setAttribute("x", fieldParam.left);
    leftRocket.setAttribute("y", fieldParam.top + fieldParam.height / 2 - leftRocketParam.height / 2);
    leftRocket.setAttribute("width", leftRocketParam.width);
    leftRocket.setAttribute("height", leftRocketParam.height);
    SVGElem.appendChild(leftRocket);

    rightRocket = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
    rightRocket.setAttribute("fill", rightRocketParam.color);
    rightRocket.setAttribute("x", fieldParam.left + fieldParam.width - rightRocketParam.width);
    rightRocket.setAttribute("y", fieldParam.top + fieldParam.height / 2 - rightRocketParam.height / 2);
    rightRocket.setAttribute("width", rightRocketParam.width);
    rightRocket.setAttribute("height", rightRocketParam.height);
    SVGElem.appendChild(rightRocket);
    //--------------------------------------------------

    //создаем мячь---------------------------------------
    ball = document.createElementNS("http://www.w3.org/2000/svg", 'circle');
    ball.setAttribute("fill", ballParam.color);
    ball.setAttribute("r", ballParam.radius);
    ball.setAttribute("cx", centreX);
    ball.setAttribute("cy", centreY);
    SVGElem.appendChild(ball);
    //--------------------------------------------------

    //подписываемся на нажатие клавиш-------------
    window.addEventListener("keydown", keyDown);
    window.addEventListener("keyup", keyUp);
    //--------------------------------------------
};//основной поток после загрузки страницы
//--------------------------------------------------------------------------------------------

//начинаем игру - запускаем мячь---------------------------------------
function gameStart() {
    if (tickTimBall) {
        clearInterval(tickTimBall);
        tickTimBall = 0;
    }
    ballCurPos.x = centreX;
    ballCurPos.y = centreY;

    ball.setAttribute("cy", ballCurPos.y);
    ball.setAttribute("cx", ballCurPos.x);
    ballGradusMove = randomDiap(0, 360); //первоначальный угол движения мяча
    ballSpeed = 3;

    tickTimBall = setInterval(moveBall, tickTim);
}

function moveBall() {
    //координаты возможных точек касания----------------------------
    var touch = {top: {x: 0, y: 0}, right: {x: 0, y: 0}, bottom: {x: 0, y: 0}, left: {x: 0, y: 0}};

    //вычисляем прогназируемую следующую координату (в svg центр мяча)
    var nextX = ballCurPos.x + ballSpeed * Math.cos(gradus(ballGradusMove));
    var nextY = ballCurPos.y + ballSpeed * Math.sin(gradus(ballGradusMove));

    touch.top.x = nextX;
    touch.top.y = nextY - ballParam.radius;
    touch.right.x = nextX + ballParam.radius;
    touch.right.y = nextY;
    touch.bottom.x = nextX;
    touch.bottom.y = nextY + ballParam.radius;
    touch.left.x = nextX - ballParam.radius;
    touch.left.y = nextY;
    //---------------------------------------------------------------

    if (touch.top.y < fieldParam.top) {
        ballGradusMove = 360 - ballGradusMove;
    } else if (touch.bottom.y > (fieldParam.top + fieldParam.height)) {
        ballGradusMove = 360 - ballGradusMove;
    }
    else if ((parseInt(touch.right.x) > parseInt(rightRocket.getAttribute("x"))) && (parseInt(touch.right.y) > parseInt(rightRocket.getAttribute("y"))) && (parseInt(touch.right.y) < (parseInt(rightRocket.getAttribute("y")) + parseInt(rightRocketParam.height)))) {
        //отбили мячь правой ракеткой
        ballGradusMove = 180 - ballGradusMove;
        ++ballSpeed;
    }
    else if ((parseInt(touch.left.x) < (parseInt(leftRocket.getAttribute("x")) + parseInt(leftRocketParam.width))) && (parseInt(touch.left.y) > parseInt(leftRocket.getAttribute("y"))) && (touch.left.y < (leftRocket.getAttribute("y") + leftRocketParam.height))) {
        //отбили мячь левой ракеткой
        ballGradusMove = 180 - ballGradusMove;
        ++ballSpeed;
    }
    else if (parseInt(touch.right.x) > (parseInt(fieldParam.left) + parseInt(fieldParam.width))) {
        //гол в правые ворота
        clearInterval(tickTimBall);
        tickTimBall = 0;
        ++score.left;
        scoreElem.textContent = `${score.left}:${score.right}`;
        return;
    }
    else if (parseInt(touch.left.x) <= parseInt(fieldParam.left)) {
        //гол в левые ворота
        clearInterval(tickTimBall);
        tickTimBall = 0;
        ++score.right;
        scoreElem.textContent = `${score.left}:${score.right}`;
        return;
    }

    ballCurPos.x = nextX;
    ballCurPos.y = nextY;
    ball.setAttribute("cy", ballCurPos.y);
    ball.setAttribute("cx", ballCurPos.x);
}

//---------------------------------------------------------------------

//обрабатываем нажатую клавишу (запуск таймера движения ракетки)-------
function keyDown(EL) {
    switch (EL.keyCode) {
        case leftRocketParam.keyMoveUp:
            if (!tickTimLeftRocket) {
                tickTimLeftRocket = setInterval(moveLeftRocketUp, tickTim);
            }
            break;
        case leftRocketParam.keyMoveDown:
            if (!tickTimLeftRocket) {
                tickTimLeftRocket = setInterval(moveLeftRocketDown, tickTim);
            }
            break;
        case rightRocketParam.keyMoveUp:
            if (!tickTimRightRocket) {
                tickTimRightRocket = setInterval(moveRightRocketUp, tickTim);
            }
            break;
        case rightRocketParam.keyMoveDown:
            if (!tickTimRightRocket) {
                tickTimRightRocket = setInterval(moveRightRocketDown, tickTim);
            }
            break;
    }
}

//-----------------------------------------------------------------------

//обрабатываем отжатую клавишу (остановка таймера движения ракетки)-------
function keyUp(EL) {
    switch (EL.keyCode) {
        case leftRocketParam.keyMoveUp:
            clearInterval(tickTimLeftRocket);
            tickTimLeftRocket = 0;
            break;
        case leftRocketParam.keyMoveDown:
            clearInterval(tickTimLeftRocket);
            tickTimLeftRocket = 0;
            break;
        case rightRocketParam.keyMoveUp:
            clearInterval(tickTimRightRocket);
            tickTimRightRocket = 0;
            break;
        case rightRocketParam.keyMoveDown:
            clearInterval(tickTimRightRocket);
            tickTimRightRocket = 0;
            break;
    }
}

//-----------------------------------------------------------------------

//перемещение ракеток----------------------------------------------------
function moveLeftRocketUp() {
    moveRocket(leftRocket, -rocketSpeed);
}

function moveLeftRocketDown() {
    moveRocket(leftRocket, rocketSpeed);
}

function moveRightRocketUp() {
    moveRocket(rightRocket, -rocketSpeed);
}

function moveRightRocketDown() {
    moveRocket(rightRocket, rocketSpeed);
}

function moveRocket(rocket, move) {
    var nextTop = parseInt(rocket.getAttribute("y")) + move; //следующее приращение
    if (nextTop >= fieldParam.top) {
        if (nextTop <= (fieldParam.top + fieldParam.height - rocket.getAttribute("height")))
            rocket.setAttribute("y", nextTop);

        //rocket.innerHTML =`<animate attributeType=XML attributeName=y from=${(rocket.getAttribute("y"))} to=${rocket.setAttribute("y",nextTop)} dur=${tickTim}ms repeatCount=1/>`;
    }
}

//-----------------------------------------------------------------------------------

//перевод градуса в радианы-------------
var gradus = function toRadians(angle) {
    return angle * (Math.PI / 180);
}
//--------------------------------------
//случайное число в диапазоне n-m
function randomDiap(n, m) {
    return Math.floor(Math.random() * (m - n + 1)) + n;
}

//--------------------------------------

