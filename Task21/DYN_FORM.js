//Переделать проект DYN_FORM на использование AJAX —
//подгружать через AJAX массив с описанием формы.
'use strict';

var form1 = document.forms['Form1'];
var form2 = document.forms['Form2'];

//Запрашиваем описание форм через AJAX--
$.ajax("dynForms.json",
    {
        type: 'GET',
        dataType: 'json',
        success: dataLoaded,
        error: errorHandler
    });
//----------------------------------

function dataLoaded(data) {
    console.log(data["formDef1"].length);
    console.log(data["formDef2"].length);

    createForm(form1, data["formDef1"]);
    createForm(form2, data["formDef2"]);
}

function errorHandler(jqXHR, statusStr, errorStr) {
    alert(statusStr + ' ' + errorStr);
}

//**************************************************************************
function createForm(frm, formDef) {
    formDef.forEach(function (v, i, a) {
        let text = document.createElement('span');

        switch (v.kind) {
            case 'longtext':
            case 'number':
            case 'shorttext':
            case 'check':
                let inp = document.createElement('input');

                text.innerHTML = `${v.label}  `;
                frm.appendChild(text);

                if (v.kind == 'longtext')
                    inp.setAttribute('type', 'text');
                if (v.kind == 'number')
                    inp.setAttribute('type', 'number');
                if (v.kind == 'shorttext')
                    inp.setAttribute('type', 'email');
                if (v.kind == 'check')
                    inp.setAttribute('type', 'checkbox');

                inp.setAttribute('name', v.name);
                frm.appendChild(inp);

                frm.appendChild(document.createElement('br'));
                break;
            case 'memo':
                let txtarea = document.createElement('textarea');

                text.innerHTML = `${v.label}  `;
                frm.appendChild(text);
                frm.appendChild(document.createElement('br'));

                txtarea.setAttribute('name', v.name);
                frm.appendChild(txtarea);

                frm.appendChild(document.createElement('br'));
                break;
            case 'combo':
                let select = document.createElement('select');
                let variants = v.variants;
                text.innerHTML = `${v.label}  `;
                frm.appendChild(text);

                select.setAttribute('name', v.name);
                select.setAttribute('style', 'width: 204px; margin-left: 2px');
                variants.forEach(function (v) {
                    select.innerHTML += `<option value="${v.value}">${v.text}</option>`;
                })
                frm.appendChild(select);

                frm.appendChild(document.createElement('br'));
                break;
            case 'radio':
                let variants_radio = v.variants;
                text.innerHTML = `${v.label}  `;
                frm.appendChild(text);

                variants_radio.forEach(function (v_r,i_r,a_r) {
                    let span = document.createElement('span');
                    let radio = document.createElement('input');

                    radio.setAttribute('type', 'radio');
                    radio.setAttribute('name', v.name);
                    radio.setAttribute('value', v_r.value);
                    frm.appendChild(radio);

                    span.innerHTML = v_r.text;
                    frm.appendChild(span);
                })
                frm.appendChild(document.createElement('br'));
                break;
            case 'submit':
                let sub = document.createElement('input');
                sub.setAttribute('type', 'submit');
                sub.setAttribute('value', v.label);

                frm.appendChild(sub);
                break;
        }
    });
}
//**************************************************************************