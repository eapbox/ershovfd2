// Ершов Александр
// Task10_e2
//Реализовать на JavaScript перетаскивание мышью по веб-странице нескольких  изображений
'use strict';
//**********************************************
var currentDiv = null;
var xy = {};
//var zIndex = 1;
var scene = document.getElementById('scene');

scene.addEventListener("mousedown", function (e) {
    //e.stopPropagation();
    currentDiv = e.target;
    currentDiv.parentNode.appendChild(currentDiv);
    // currentDiv.style.zIndex = ++zIndex;
    var s = getComputedStyle(currentDiv);
    xy.top = s.top;
    xy.left = s.left;
    xy.mtop = e.clientY;
    xy.mleft = e.clientX;
});
scene.onmousemove = function (e) {
    if (currentDiv) {
        currentDiv.style.top = parseInt(xy.top) + (e.clientY - xy.mtop) + 'px';
        currentDiv.style.left =  parseInt(xy.left) + (e.clientX - xy.mleft) + 'px';
    }
    //currentDiv.innerHTML = `x:${e.screenX}; y:${e.screenY}`;
}

scene.onmouseup = function (e) {
    currentDiv = null;
    xy = {};
}
scene.onmouseout = function (e) {
    currentDiv = null;
    xy = {};
    //currentDiv.innerHTML = ``;
}
//**********************************************