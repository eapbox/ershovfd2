// Ершов Александр
// Task10_e1
'use strict';

var formTag = document.forms['INFO'];
formTag.addEventListener('submit', validateForm, false); // назначаем обработчик события submit

var inputAuthor = document.getElementById('inputAuthor');
var inputAuthorErr = document.getElementById('inputAuthorErr');
inputAuthor.addEventListener('blur', validInputAuthor, false); //назначение обработчика при покидании поля ввода

var siteName = document.getElementById('siteName');
var siteNameErr = document.getElementById('siteNameErr');
siteName.addEventListener('blur', validSiteName, false); //назначение обработчика при покидании поля ввода

var siteUrl = document.getElementById('siteUrl');
var siteUrlErr = document.getElementById('siteUrlErr');
siteUrl.addEventListener('blur', validsiteUrl, false); //назначение обработчика при покидании поля ввода


var siteDate = document.getElementById('siteDate');
var siteDateErr = document.getElementById('siteDateErr');
siteDate.addEventListener('blur', validDate, false); //назначение обработчика при покидании поля ввода

var userCount = document.getElementById('userCount');
var userCountErr = document.getElementById('userCountErr');
userCount.addEventListener('blur', validUserCount, false); //назначение обработчика при покидании поля ввода

var email = document.getElementById('email');
var emailErr = document.getElementById('emailErr');
email.addEventListener('blur', validEmail, false); //назначение обработчика при покидании поля ввода

var publicField = formTag.elements['public'];
var publicErr = document.getElementById('publicErr');

//--------------------------------------------
function validInputAuthor() {
    var str = inputAuthor.value;
    if (!isNaN(Number(str))) {
        inputAuthorErr.style.color = 'red';
        inputAuthorErr.innerHTML = 'Ошибка формата';
        return false;
    }
    inputAuthorErr.style.color = 'green';
    inputAuthorErr.innerHTML = '+';
    return true;
}

function validSiteName() {
    var str = siteName.value;
    if (!isNaN(Number(str))) {
        siteNameErr.style.color = 'red';
        siteNameErr.innerHTML = 'Ошибка формата';
        return false;
    }
    siteNameErr.style.color = 'green';
    siteNameErr.innerHTML = '+';
    return true;
}

function validsiteUrl() {
    var str = siteUrl.value;
    if (!validateUrl(str)) {
        siteUrlErr.style.color = 'red';
        siteUrlErr.innerHTML = 'Ошибка формата';
        return false;
    }
    siteUrlErr.style.color = 'green';
    siteUrlErr.innerHTML = '+';
    return true;
}

function validateUrl(value) {
    return /^(ftp|http|https):\/\/[^ "]+$/.test(value);
}

function validDate() {
    var str = siteDate.value;
    if (!validateDate(str)) {
        siteDateErr.style.color = 'red';
        siteDateErr.innerHTML = 'Ошибка формата';
        return false;
    }
    siteDateErr.style.color = 'green';
    siteDateErr.innerHTML = '+';
    return true;
}

function validateDate(dt) {
    return /^\d{2}.\d{2}.\d{4}$/.test(dt);
}

function validUserCount() {
    var str = userCount.value;
    if (isNaN(Number(str))) {
        userCountErr.style.color = 'red';
        userCountErr.innerHTML = 'Ошибка формата';
        return false;
    }
    userCountErr.style.color = 'green';
    userCountErr.innerHTML = '+';
    return true;
}

function validEmail() {
    var str = email.value;
    if (!validateEmail(str)) {
        emailErr.style.color = 'red';
        emailErr.innerHTML = 'Ошибка формата';
        return false;
    }
    emailErr.style.color = 'green';
    emailErr.innerHTML = '+';
    return true;
}

function validateEmail(em) {
    return /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(em);
}

//--------------------------------------------

//валидируем форму цкликом перед отправкой--------------------------------------------
function validateForm(EO) {
    var publicValue = publicField.value; // строковое значение
    var rezValidate = true;
    var countErr = 0;

    function setErr(el) {
        if (countErr == 0) {
            countErr++;
            el.scrollIntoView();
            el.focus();
        }
        rezValidate = false;
    }

    if (!validInputAuthor()) setErr(inputAuthor);
    if (!validSiteName()) setErr(siteName);
    if (!validsiteUrl()) setErr(siteUrl);
    if (!validDate()) setErr(siteDate);
    if (!validUserCount()) setErr(userCount);
    if (!validEmail()) setErr(email);

    if (publicValue == "") { // если ничего не выбрано - radio показывает пустую строку
        publicErr.style.color = 'red';
        publicErr.innerHTML = 'Не выбрано размещение!';
        if (countErr == 0) {
            countErr++;
            document.getElementById('publicFirst').scrollIntoView();
        }
        rezValidate = false;
    } else {
        publicErr.style.color = 'green';
        publicErr.innerHTML = '+';
    }

    if (!rezValidate) EO.preventDefault(); // форма не будет отправлена на сервер
}
//-----------------------------------------------------------------------------------