'use strict';
var body = document.body;
var clock = document.getElementById('clock');
var clockSt = getComputedStyle(clock);
var centrX, centrY;

//перевод градуса в радианы-------------
var rad = function toRadians(angle) {
    return angle * (Math.PI / 180);
}
//--------------------------------------

//вычисляем координаты центра циферблата-------------------------
centrX = parseInt(clockSt.left) + (parseInt(clockSt.width) / 2);
centrY = parseInt(clockSt.top) + (parseInt(clockSt.height) / 2);
//---------------------------------------------------------------

//формируем цифры часов------------------------------------------
for (var i = 0; i < 12; i++) {
    let dig = document.createElement('div');
    let x, y;
    dig.className = 'digits';
    dig.innerHTML = i + 1;
    body.appendChild(dig);
    let digSt = getComputedStyle(dig);

    x = centrX + ((parseInt(clockSt.width) / 2) - 40) * Math.cos(rad(30) * i - rad(60)) - (parseInt(digSt.width) / 2);
    y = centrY + ((parseInt(clockSt.width) / 2) - 40) * Math.sin(rad(30) * i - rad(60)) - (parseInt(digSt.width) / 2);
    dig.style.left = x + 'px';
    dig.style.top = y + 'px';
}
//---------------------------------------------------------------

//создаем элементы стрелок часов---------------------------------
let lineHour = document.createElement('div');
lineHour.className = 'lineHour';
body.appendChild(lineHour);
lineHour.style.top = (centrY-20) + 'px';
lineHour.style.left = (centrX-3) + 'px';

let lineMin = document.createElement('div');
lineMin.className = 'lineMin';
body.appendChild(lineMin);
lineMin.style.top = (centrY-20) + 'px';
lineMin.style.left = (centrX-2) + 'px';

let lineSec = document.createElement('div');
lineSec.className = 'lineSec';
body.appendChild(lineSec);
lineSec.style.top = (centrY-20) + 'px';
lineSec.style.left = (centrX) + 'px';
//---------------------------------------------------------------

//запускаем таймер-----------------------------------------------
var digitalClock = document.getElementById('digitalClock');
setInterval(updateTime, 1000);
var countSec = 0;

function updateTime() {
    var currTime = new Date();
    var hours = currTime.getHours();
    var minutes = currTime.getMinutes();
    var seconds = currTime.getSeconds();
    digitalClock.innerHTML = str0l(hours, 2) + ':' + str0l(minutes, 2) + ':' + str0l(seconds, 2);

    lineSec.style.transform = `rotate(${180+6*(seconds+1+countSec)}deg)`;
    if (seconds == 59)
        countSec +=60;

    lineMin.style.transform = `rotate(${180+6*minutes + Math.ceil(0.1*seconds)}deg)`;
    lineHour.style.transform = `rotate(${180+30*hours + Math.ceil(0.5*minutes)}deg)`;
}

// дополняет строку val слева нулями до длины Len
function str0l(val, len) {
    var strVal = val.toString();
    while (strVal.length < len)
        strVal = '0' + strVal;
    return strVal;
}
//---------------------------------------------------------------





