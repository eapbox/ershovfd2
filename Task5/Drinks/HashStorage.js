'use strict';
function HashStorage () {
    var self = this;
    var storage = {};


    self.addValue = function(key,value) {
        storage[key.toString()] = value;
    }
    self.getValue = function(key) {
        return storage[key.toString()];
    }
    self.deleteValue = function(key) {
        if (key in storage) {
            delete storage[key.toString()]
            return true;
        }
        return false;
    }
    self.getKeys = function () {
        var keys = [];
        for (var key in storage) {
            keys.push(key);
        }
        return keys;
    }
}
//**********************************************************

var drinkStorage = new HashStorage();
var message = document.getElementById('message');

function createDrink() {
    var name = prompt('Введите название напитка: ');
    var isAlch = confirm('Алкогольный?');
    var recipe = prompt('Рецепт приготовления: ');

    drinkStorage.addValue(name, {isAlch:isAlch.toString(), recipe: recipe.toString()});
}

function getInfo() {
    var name = prompt('Введите название напитка: ');
    var drink = drinkStorage.getValue(name);

    if (drink) {
        message.innerHTML = `напиток: ${name}&#13;&#10;алкогольный: ${drink.isAlch}&#13;&#10;рецепт приготовления:&#13;&#10;${drink.recipe}`;
        return;
    }
    message.innerHTML = `напитка: ${name} - нет!`;
}

function deleteDrink() {
    var name = prompt('Введите название напитка: ');
    if (drinkStorage.deleteValue(name)) {
        message.innerHTML = `напиток: ${name} - успешно удален!`;
        return;
    }
    message.innerHTML = `напитка: ${name} - нет!`;
}

function listDrinks() {
    var drinks = drinkStorage.getKeys();
    var list = '';
    for (var el of drinks) {
        list +=el + '&#13;&#10;';
    }
    message.innerHTML = list;
}

