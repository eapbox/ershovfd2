// Ершов Александр
// Task5_C1
// без повтора цветов при выводе
'use strict';

function randomDiap(n,m) {
    return Math.floor(Math.random()*(m-n+1))+n;
}

function mood(colorsCount) {
    console.log( 'цветов: ' + colorsCount );
    var colors=[ '', 'красный', 'оранжевый', 'жёлтый', 'зелёный', 'голубой', 'синий', 'фиолетовый' ];
    var used={};
    var originalCount = 0;

    while (originalCount < colorsCount) {
        var n=randomDiap(1,7);
        if ( n in used )
            continue;

        used[n] = true;
        var colorName=colors[n];
        console.log( colorName );
        originalCount++;
    }
}
mood(3);